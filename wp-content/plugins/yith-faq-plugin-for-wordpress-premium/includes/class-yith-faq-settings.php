<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH FAQ plugin for WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_FAQ_Settings' ) ) {

	/**
	 * Manages plugin settings
	 *
	 * @class   YITH_FAQ_Settings
	 * @since   1.0.0
	 * @author  Your Inspiration Themes
	 *
	 * @package Yithemes
	 */
	class YITH_FAQ_Settings {

		/**
		 * Options array
		 *
		 * @var array
		 */
		private $options = array();

		/**
		 * Single instance of the class
		 *
		 * @since 1.0.0
		 * @var YITH_FAQ_Settings
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @return YITH_FAQ_Settings
		 * @since 1.0.0
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * Constructor
		 *
		 * @return  void
		 * @since   1.0.0
		 * @author  Alberto Ruggiero <alberto.ruggiero@yithemes.com>
		 */
		private function __construct() {
		}

		/**
		 * Load plugin options
		 *
		 * @param string $parent The option parent container.
		 *
		 * @return  array
		 * @since   1.0.0
		 * @author  Alberto Ruggiero <alberto.ruggiero@yithemes.com>
		 */
		private function get_options( $parent ) {

			if ( ! isset( $this->options[ $parent ] ) ) {

				$options = get_option( "yit_{$parent}_options" );

				if ( '' === $options ) {
					$options = array();
					update_option( "yit_{$parent}_options", array() );
				}

				$this->options[ $parent ] = $options;

			}

			return $this->options[ $parent ];

		}

		/**
		 * Get selected option
		 *
		 * @param string $parent  The option parent container.
		 * @param string $key     The option key.
		 * @param mixed  $default The default value.
		 *
		 * @return  mixed
		 * @since   1.0.0
		 * @author  Alberto Ruggiero <alberto.ruggiero@yithemes.com>
		 */
		public function get_option( $parent, $key, $default = false ) {
			$options = $this->get_options( $parent );

			return is_array( $options ) && array_key_exists( $key, $options ) ? $options[ $key ] : $default;
		}

	}

}
