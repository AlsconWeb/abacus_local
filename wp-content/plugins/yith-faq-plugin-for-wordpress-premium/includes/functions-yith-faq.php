<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH FAQ plugin for WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! function_exists( 'yfwp_get_option' ) ) {

	/**
	 * Get plugin option
	 *
	 * @param string $option  The option name.
	 * @param mixed  $default The default value.
	 *
	 * @return  mixed
	 * @since   1.0.0
	 *
	 * @author  Alberto Ruggiero <alberto.ruggiero@yithemes.com>
	 */
	function yfwp_get_option( $option, $default = false ) {
		return YITH_FAQ_Settings::get_instance()->get_option( 'faq', $option, $default );
	}
}

if ( ! function_exists( 'yfwp_get_categories' ) ) {

	/**
	 * Get FAQ Categories
	 *
	 * @return  array
	 * @since   1.1.5
	 *
	 * @author  Alberto Ruggiero <alberto.ruggiero@yithemes.com>
	 */
	function yfwp_get_categories() {
		$categories = get_terms(
			array(
				'taxonomy'   => YITH_FWP()->taxonomy,
				'hide_empty' => false,
			)
		);

		$terms = array();
		if ( $categories ) {
			foreach ( $categories as $category ) {
				$terms[ $category->term_id ] = $category->name;
			}
		}

		return $terms;
	}
}

if ( ! function_exists( 'yfwp_get_elementor_item_for_page' ) ) {

	/**
	 * Check if a page has a determined Elementor widget
	 *
	 * @param string  $item_id      The widget identifier (set with get_name() function in widget class).
	 * @param integer $post_id      The Post ID where we can check if the widget is used.
	 * @param boolean $get_settings Choose to get the widget settings.
	 *
	 * @return bool|array
	 * @since   1.1.5
	 *
	 * @author  Alberto Ruggiero <alberto.ruggiero@yithemes.com>
	 */
	function yfwp_get_elementor_item_for_page( $item_id, $post_id, $get_settings = false ) {

		// Check if Elementor is enabled.
		if ( defined( 'ELEMENTOR_VERSION' ) ) {

			// Check if page is built with Elementor.
			if ( \Elementor\Plugin::$instance->db->is_built_with_elementor( $post_id ) ) {
				if ( ! $get_settings ) {

					// If i only want to check if the Elementor widget is used on the page.
					$meta = get_post_meta( $post_id, '_elementor_controls_usage', true );

					if ( is_array( $meta ) && array_key_exists( $item_id, $meta ) ) {
						return true;
					}
				} else {

					// If i want to get the Elementor widget settings.
					$meta = get_post_meta( $post_id, '_elementor_data', true );
					if ( is_string( $meta ) && ! empty( $meta ) ) {
						$meta = json_decode( $meta, true );
					}

					if ( ! empty( $meta ) ) {

						$item_settings = false;

						\Elementor\Plugin::$instance->db->iterate_data(
							$meta,
							function ( $element ) use ( $item_id, &$item_settings ) {
								if ( ! empty( $element['widgetType'] ) && $item_id === $element['widgetType'] ) {
									$item_settings = $element['settings'];
								}

								return $element;
							}
						);

						return $item_settings;
					}
				}
			}
		}

		return false;
	}
}

$deprecated_filters_map = array(
	'yfwp_instantiate_shortcode_button' => array(
		'since'  => '1.1.5',
		'use'    => 'yith_faq_instantiate_shortcode_button',
		'params' => 1,
	),
	'yith_fwp_rewrite'                  => array(
		'since'  => '1.1.5',
		'use'    => 'yith_faq_rewrite',
		'params' => 1,
	),
	'yith_fwp_needs_flushing'           => array(
		'since'  => '1.1.5',
		'use'    => 'yith_faq_needs_flushing',
		'params' => 1,
	),
	'yfwp_add_scripts'                  => array(
		'since'  => '1.1.5',
		'use'    => 'yith_faq_add_scripts',
		'params' => 2,
	),
	'yith_fwp_search_placeholder'       => array(
		'since'  => '1.1.5',
		'use'    => 'yith_faq_search_placeholder',
		'params' => 1,
	),

);

foreach ( $deprecated_filters_map as $deprecated_filter => $options ) {
	$new_filter = $options['use'];
	$params     = $options['params'];
	$since      = $options['since'];
	add_filter(
		$new_filter,
		function () use ( $deprecated_filter, $since, $new_filter ) {
			$args = func_get_args();
			$r    = $args[0];

			if ( has_filter( $deprecated_filter ) ) {
				error_log( sprintf( 'Deprecated filter: %s since %s. Use %s instead!', $deprecated_filter, $since, $new_filter ) ); //phpcs:ignore

				$r = call_user_func_array( 'apply_filters', array_merge( array( $deprecated_filter ), $args ) );
			}

			return $r;
		},
		10,
		$params
	);
}
