=== YITH FAQ Plugin for WordPress Premium ===

Contributors: yithemes
Tags: faq, faqs, yit, yith, yithemes, e-commerce, shop, frequently asked questions
Requires at least: 5.4
Tested up to: 5.7
Stable tag: 1.2.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.2.4 - Released: 15 April 2021 =

* Update: YITH plugin framework

= 1.2.3 - Released: 05 March 2021 =

* New: support for WordPress 5.7
* Update: YITH plugin framework

= 1.2.2 - Released: 03 February 2021 =

* Update: YITH plugin framework

= 1.2.1 - Released: 07 January 2021 =

* Update: YITH plugin framework

= 1.2.0 - Released: 02 December 2020 =

* Update: YITH plugin framework

= 1.1.9 - Released: 24 November 2020 =

* New: German Language
* Update: YITH plugin framework
* Fix: pagination issue

= 1.1.8 - Released: 09 November 2020 =

* New: support for WordPress 5.6.x
* New: Possibility to update plugin via WP-CLI
* Update: YITH plugin framework

= 1.1.7 - Released: 21 September 2020 =

* Update: YITH plugin framework

= 1.1.6 - Released: 18 August 2020 =

* New: support for WordPress 5.5
* Update: YITH plugin framework
* Fix: category filter not working on Elementor

= 1.1.5 - Released: 24 June 2020 =

* New: FAQ block for Elementor
* Tweak: improved admin panel and settings UX
* Dev: code refactoring of the entire plugin
* Update: YITH plugin framework
* Update: Italian language
* Update: Spanish language
* Update: Dutch language

= 1.1.4 - Released: 29 April 2020 =

* Update: YITH plugin framework

= 1.1.3 - Released: 26 February 2020 =

* New: support for WordPress 5.4
* Update: YITH plugin framework

= 1.1.2 - Released: 10 November 2019 =

* Update: YITH plugin framework

= 1.1.1 - Released: 30 October 2019 =

* Update: YITH plugin framework

= 1.1.0 - Released: 23 October 2019 =

* New: support for WordPress 5.3 RC2
* Update: YITH plugin framework
* Update: Spanish language

= 1.0.9 - Released: 30 July 2019 =

* Update: Italian language
* Update: YITH plugin framework

= 1.0.8 - Released: 29 May 2019 =

* New: support for WordPress 5.2
* Update: YITH plugin framework

= 1.0.7 - Released: 29 January 2019 =

* Update: YITH plugin framework

= 1.0.6 - Released: 10 January 2019 =

* Update: YITH plugin framework
* Fix: fixed category filter CSS
* Dev: added filter yith_faq_enable_scroll

= 1.0.5 - Released: 07 December 2018 =

* New: support for WordPress 5.0
* New: Gutenberg block for FAQ shortcode
* Update: YITH plugin framework

= 1.0.4 - Released: 22 October 2018 =

* Update: YITH plugin framework

= 1.0.3 - Released: 04 October 2018 =

* Fix: scripts conflicting with other plugins scripts

= 1.0.2 - Released: 02 October 2018 =

* New: Italian translation
* Update: YITH plugin framework

= 1.0.1 - Released: 31 August 2018 =

* New: Dutch translation
* Update: YITH plugin framework
* Fix: pagination links behavior

= 1.0.0 - Released: 30 July 2018 =

* Initial Release
