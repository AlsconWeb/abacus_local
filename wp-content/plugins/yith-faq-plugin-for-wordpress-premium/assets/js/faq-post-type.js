/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH FAQ plugin for WordPress
 */

jQuery(
	function ( $ ) {

		$( '.column-enable .on_off' ).change(
			function () {

				var container = $( this ).parent();
				var data      = {
					action : 'yfwp_enable_switch',
					faq_id : $( this ).attr( 'id' ).replace( 'enable_', '' ),
					enabled: $( this ).val()
				};

				container.addClass( 'processing' );
				container.block(
					{
						message   : null,
						overlayCSS: {
							background: '#fff',
							opacity   : 0.6
						}
					}
				);

				$.post(
					yith_faq_post_type.ajax_url,
					data,
					function () {

						container
							.removeClass( 'processing' )
							.unblock();

					}
				);

			}
		);

		if ( ! yith_faq_post_type.is_order_by ) {

			$( 'table.posts #the-list, table.pages #the-list' ).sortable(
				{
					'items' : 'tr',
					'axis'  : 'y',
					'helper': function ( e, ui ) {
						ui.children().children().each(
							function () {
								$( this ).width( $( this ).width() );
							}
						);
						return ui;
					},
					'update': function () {
						$.post(
							yith_faq_post_type.ajax_url,
							{
								action: 'yfwp_order_faqs',
								order : $( '#the-list' ).sortable( 'serialize' )
							}
						);
					}
				}
			);

		}

	}
);
