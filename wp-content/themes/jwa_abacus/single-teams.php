<?php
/**
 * Created 01.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */

get_header();

$id        = get_the_ID();
$spec      = get_field( 'speciality', $id );
$soc       = get_field( 'soc', $id );
$currentID = $id;

$terms = get_terms( [
	'taxonomy'   => 'position',
	'hide_empty' => true,
	'orderby'    => 'id',
	'order'      => 'ASC',
] );

$allTeam = [];
if( ! empty( $terms ) ) {
	foreach ( $terms as $term ) {
		array_push( $allTeam, getAllTeamByTerm( $term->term_id ) );
	}
}

$resultTeam = [];
array_walk_recursive( $allTeam, function ( $item, $key ) use ( &$resultTeam ) {
	$resultTeam[] = $item;
} );
?>
	
	<section>
		<div class="inner-team">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h2>Team of Professionals</h2>
						<div class="dfr">
							<?php if( have_posts() ): ?>
						<?php while ( have_posts() ):
						the_post(); ?>
							<div class="img">
								<?php if( has_post_thumbnail( $id ) ): the_post_thumbnail( 'team_photo' ); ?>
								<?php else: ?>
									<img src="<?php echo get_field( 'photo', $id ); ?>" alt="#">
								<?php endif; ?>
							</div>
							<div class="desc">
								<h3><?php the_title(); ?></h3>
								<?php $termName = wp_get_post_terms( $id, 'position' )[0]->name; ?>
								<h4><?php echo $termName ?></h4>
								<?php if( $spec ): ?>
									<ul class="meta">
										<?php foreach ( $spec as $item ): ?>
											<li><?php echo $item; ?></li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
								<?php echo get_field( 'excerpt', $id ); ?>
								<?php if( $soc ): ?>
									<ul class="soc">
										<?php foreach ( $soc as $item ): ?>
											<li class="icon-<?php echo $item['icon_name'] ?>"><a href="<?php echo $item['link'] ?>"></a></li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
								<?php $pageID = get_page_by_title( 'Team of Professionals', OBJECT, 'page' ) ?>
								<a class="back icon-left-arrow" href="<?php echo get_the_permalink( $pageID ) ?>">Back to Team</a>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
							
							<?php
							$arg   = [
								'post_type'      => 'teams',
								'posts_per_page' => - 1,
								'post__in'       => $resultTeam,
								'orderby'        => 'post__in',
								'order'          => 'ASC',
							];
							$query = new WP_Query( $arg );
							?>
							<div class="partners slider">
								<?php if( $query->have_posts() ): $i = 0; ?>
									<?php while ( $query->have_posts() ):
										$query->the_post();
										$id = get_the_ID(); ?>
										<div class="partner <?php echo $currentID === $id ? 'active' : ''; ?>" data-current="<?php echo
										$currentID === $id ? 'true' : ''; ?>" data-index_position="<?php echo $i; ?>">
											<div class="img">
												<?php if( has_post_thumbnail( $id ) ): the_post_thumbnail( 'team_photo' ); ?>
												<?php else: ?>
													<img src="<?php echo get_field( 'photo', $id ); ?>" alt="#">
												<?php endif; ?>
											</div>
											<h3><?php the_title(); ?></h3>
											<?php $termName = wp_get_post_terms( $id, 'position' )[0]->name; ?>
											<h4><?php echo $termName ?></h4>
											<a class="link" href="<?php the_permalink(); ?>"></a>
										</div>
										<?php $i ++; endwhile;
									wp_reset_postdata(); ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>