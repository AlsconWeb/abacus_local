<?php
/*
* Created 11.03.20
* Version 1.0.0
* Last update 11.03.20
* Author: Maxym Y
*/

add_action( 'wp_enqueue_scripts', 'scripts_init' );
function scripts_init() {
	wp_enqueue_style( 'abacus-style-css', get_template_directory_uri() . '/style.css', [], version( '/style.css' ) );
	wp_enqueue_style( 'abacus-main-css', get_template_directory_uri() . '/assets/css/main.css', [], version( '/assets/css/main.css' ) );
	wp_enqueue_script( 'abacus-build-js', get_template_directory_uri() . '/assets/js/build.js', [], version( '/assets/js/build.js' ), true );
	wp_enqueue_script( 'abacus-main_sdk-js', get_template_directory_uri() . '/assets/js/main_sdk.js', [ 'jquery' ], version( '/assets/js/main_sdk.js' ), true );
	wp_enqueue_script( 'abacus-map-js', 'https://maps.googleapis.com/maps/api/js?key=' . get_field( 'g_key', 'option' ) . '&amp;amp;callback=initMap' );
	wp_enqueue_script( 'abacus-map-assets-js', get_template_directory_uri() . '/assets/js/map.js', [], version( '/assets/js/map.js' ), true );
	wp_enqueue_style( 'abacus-font-css', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap', [] );
	wp_enqueue_script( 'abacus-player-js', get_template_directory_uri() . '/assets/js/player.js', [], version( '/assets/js/player.js' ), true );
	wp_enqueue_style( 'sweetalert2_css', 'https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.min.css' );
	wp_enqueue_script( 'sweetalert2', 'https://cdn.jsdelivr.net/npm/sweetalert2@9', [ 'jquery' ], '', false );
}

add_action( 'admin_enqueue_scripts', 'add_style_to_admin_page' );
function add_style_to_admin_page( $hook_suffix ) {
	if( $hook_suffix == 'toplevel_page_quickbooks' ) {
		wp_enqueue_style( 'bootstrap_4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' );
		wp_enqueue_style( 'sweetalert2_css', 'https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.min.css' );
		wp_enqueue_script( 'sweetalert2', 'https://cdn.jsdelivr.net/npm/sweetalert2@9', [ 'jquery' ], '', false );
		wp_enqueue_script( 'abacus-bq-auth', get_template_directory_uri() . '/include/admin/js/bq_auth.js', [ 'jquery' ],
			version( '/include/admin/js/bq_auth.js' ) );
	}
}

function version( $src ) {
	return filemtime( get_stylesheet_directory() . $src );
}

add_action( 'widgets_init', 'register_abacus_mail_widget' );

function register_abacus_mail_widget() {
	register_widget( 'newsletter_widget' );
	register_widget( 'recentnews_widget' );
}

add_action( 'after_setup_theme', 'init_theme' );
function init_theme() {
	define( 'ALLOW_UNFILTERED_UPLOADS', true );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	
	require_once dirname( __FILE__ ) . '/include/widgets/newsletter_widget.php';
	require_once dirname( __FILE__ ) . '/include/widgets/recentnews_widget.php';
	
	// add custom thumbnails
	add_image_size( 'blog_thumbnail', 440, 250, true );
	add_image_size( 'blog_thumbnail_2', 660, 375, true );
	add_image_size( 'blog_thumbnail_big_first', 920, 337, true );
	add_image_size( 'blog_thumbnail_big_second', 440, 337, true );
	add_image_size( 'blog_thumbnail_small_first', 202, 132, true );
	add_image_size( 'blog_thumbnail_small_second', 440, 287, true );
	add_image_size( 'blog_thumbnail_small_widget', 115, 75, true );
	add_image_size( 'blog_thumbnail_main', 1040, 550, true );
	add_image_size( 'team_photo', 329, 329, false );
	// register menu
	register_nav_menus( [
		'top_menu'    => 'Top-menu',
		'footer_menu' => 'Footer-menu',
	] );
}

add_action( 'vc_before_init', 'wpc_vc_before_init_actions' );
function wpc_vc_before_init_actions() {
// Require new custom Element
	require_once __DIR__ . '/include/jwa_components/main_banner/main_banner.php';
	require_once __DIR__ . '/include/jwa_components/second_banner/second_banner.php';
	require_once __DIR__ . '/include/jwa_components/clients_logo_slider/clients_logo_slider.php';
	require_once __DIR__ . '/include/jwa_components/team_slider/team_slider.php';
	require_once __DIR__ . '/include/jwa_components/our_services/our_services.php';
	require_once __DIR__ . '/include/jwa_components/our_mission/our_mission.php';
	require_once __DIR__ . '/include/jwa_components/latest_blog/latest_blog.php';
	require_once __DIR__ . '/include/jwa_components/podcast/podcast.php';
	require_once __DIR__ . '/include/jwa_components/news_blog/news_blog.php';
	require_once __DIR__ . '/include/jwa_components/content_and_form/content_and_form.php';
	require_once __DIR__ . '/include/jwa_components/tools_and_technology/tools_and_technology.php';
	require_once __DIR__ . '/include/jwa_components/contact_us_form/contact_us_form.php';
	require_once __DIR__ . '/include/jwa_components/portal_client/portal_client.php';
	require_once __DIR__ . '/include/jwa_components/team_elem/jwa_team_elem.php';
	require_once __DIR__ . '/include/jwa_components/team_banner/iwp_team_banner.php';
	require_once __DIR__ . '/include/jwa_components/team_new/iwp_team_new.php';
}

/**
 * mime_types
 * add support svg file
 * Version 1.0.0
 */
function mime_types( $mime_types ) {
	$mime_types['svg'] = 'image/svg+xml';
	
	return $mime_types;
}

add_filter( 'upload_mimes', 'mime_types', 1, 1 );

if( function_exists( 'acf_add_options_page' ) ) {
	
	acf_add_options_page( [
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false,
	] );
}

function init_google_map() {
	$key = get_field( 'g_key', 'option' );
	acf_update_setting( 'google_api_key', $key );
}

add_action( 'acf/init', 'init_google_map' );

require_once dirname( __FILE__ ) . '/include/admin/cpt/add_team.php';

//Create multi dropdown param type
vc_add_shortcode_param( 'dropdown_multi', 'dropdown_multi_settings_field' );
function dropdown_multi_settings_field( $param, $value ) {
	$param_line = '';
	$param_line .= '<select multiple name="' . esc_attr( $param['param_name'] ) . '" class="wpb_vc_param_value wpb-input wpb-select ' . esc_attr( $param['param_name'] ) . ' ' . esc_attr( $param['type'] ) . '">';
	foreach ( $param['value'] as $text_val => $val ) {
		if( is_numeric( $text_val ) && ( is_string( $val ) || is_numeric( $val ) ) ) {
			$text_val = $val;
		}
		$text_val = __( $text_val, "js_composer" );
		$selected = '';
		
		if( ! is_array( $value ) ) {
			$param_value_arr = explode( ',', $value );
		} else {
			$param_value_arr = $value;
		}
		
		if( $value !== '' && in_array( $val, $param_value_arr ) ) {
			$selected = ' selected="selected"';
		}
		$param_line .= '<option class="' . $val . '" value="' . $val . '"' . $selected . '>' . $text_val . '</option>';
	}
	$param_line .= '</select>';
	
	return $param_line;
}

function abacus_excerpt( $limit, $post ) {
	$excerpt = substr( get_the_excerpt( $post ), 0, $limit );
	
	if( strlen( $excerpt ) >= $limit ) {
		//array_pop($excerpt);
		$excerpt = $excerpt . '...';
	}
	
	$excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );
	
	return $excerpt;
}

if( ! function_exists( 'mit_thumbnail_upscale' ) ) {
	function mit_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ) {
		
		if( ! $crop ) {
			return null;
		} // let the wordpress default function handle this
		
		$aspect_ratio = $orig_w / $orig_h;
		$size_ratio   = max( $new_w / $orig_w, $new_h / $orig_h );
		
		$crop_w = round( $new_w / $size_ratio );
		$crop_h = round( $new_h / $size_ratio );
		
		$s_x = floor( ( $orig_w - $crop_w ) / 2 );
		$s_y = floor( ( $orig_h - $crop_h ) / 2 );
		
		return [ 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h ];
	}
}
add_filter( 'image_resize_dimensions', 'mit_thumbnail_upscale', 10, 6 );

function custom_excerpt_length( $length ) {
	return 999;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 10 );

function custom_excerpt_more_link( $more ) {
	return '';
}

add_filter( 'excerpt_more', 'custom_excerpt_more_link' );


//attach our function to the wp_pagenavi filter
add_filter( 'wp_pagenavi', 'wd_pagination', 10, 2 );

//customize the PageNavi HTML before it is output
function wd_pagination( $html ) {
	$out = '';
	$out = str_replace( 'wp-pagenavi', 'nav-links', $html );
	$out = str_replace( 'current', 'page-numbers current', $out );
	$out = str_replace( 'page', 'page-numbers page', $out );
	$out = str_replace( 'nextpostslink', 'page-numbers next', $out );
	$out = str_replace( 'previouspostslink', 'page-numbers prev', $out );
	$out = str_replace( 'extend', 'page-numbers dots', $out );
	
	return $out;
}

add_action( 'widgets_init', 'abacus_widgets' );
function abacus_widgets() {
	register_sidebar( [
		'name'          => 'News sidebar',
		'id'            => "news-sidebar",
		'description'   => '',
		'class'         => '',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	] );
}

//add_filter( 'woocommerce_checkout_create_order', 'add_invoice_name_to_order_title' );
//function add_invoice_name_to_order_title ( $order ) {
////	$order->post->post_title = $order->post->post_title . 'Invoce#';
//	var_dump( $order->data );
//	die();
//}

// comment form fields re-defined:
add_filter( 'comment_form_default_fields', 'mo_comment_fields_custom_html' );
function mo_comment_fields_custom_html( $fields ) {
	// first unset the existing fields:
	unset( $fields['comment'] );
	unset( $fields['author'] );
	unset( $fields['email'] );
	unset( $fields['url'] );
	// then re-define them as needed:
	$fields = [
		'comment_field' => '
    <div class="textarea label">
      <label>Comment</label>
      <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea>
    </div>
    <p class="comment-form-comment textarea"><label for="comment">' . _x( 'A CUSTOM COMMENT LABEL', 'noun', 'textdomain' ) . '</label> ' .
		                   '<textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p>',
		'author'        => '<p class="comment-form-author">' . '<label for="author">' . __( 'A CUSTOM NAME LABEL', 'textdomain' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		                   '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245"' . $aria_req . $html_req . ' /></p>',
		'email'         => '<p class="comment-form-email"><label for="email">' . __( 'A CUSTOM EMAIL LABEL', 'textdomain' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		                   '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req . ' /></p>',
		'url'           => '<p class="comment-form-url"><label for="url">' . __( 'A CUSTOM WEBSITE LABEL', 'textdomain' ) . '</label> ' .
		                   '<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></p>',
	];
	
	// done customizing, now return the fields:
	return $fields;
}

function parseAuthRedirectUrl( $url ) {
	parse_str( $url, $qsArray );
	
	return [
		'code'    => $qsArray['code'],
		'realmId' => $qsArray['realmId'],
	];
}

// add SDK QuickBooks
require_once dirname( __FILE__ ) . '/include/sdk/vendor/autoload.php';
require dirname( __FILE__ ) . '/include/admin/ajax/get_invoice.php';
require dirname( __FILE__ ) . '/include/admin/ajax/qb_auth.php';
require dirname( __FILE__ ) . '/include/admin/ajax/qb_save_access.php';
require dirname( __FILE__ ) . '/include/admin/cron/qb_token_update.php';

// add option page
require_once dirname( __FILE__ ) . '/include/admin/page/quickbooks/settings.php';

// remove field adn action woo
require_once dirname( __FILE__ ) . '/include/admin/woo_hooks/remove_action.php';

// redirect to checkout
require_once dirname( __FILE__ ) . '/include/admin/woo_hooks/redirect_to_checkout.php';

// update invoice after payment
require_once dirname( __FILE__ ) . '/include/admin/woo_hooks/update_invoice_after_payment.php';

// add filter payment button
require_once dirname( __FILE__ ) . '/include/admin/woo_filters/place_order.php';

// send invoice
require_once dirname( __FILE__ ) . '/include/admin/action/send_emails.php';


add_filter( 'yith_faq_add_scripts', '__return_true' );

function abacus_post_types() {
	register_post_type( 'position', [
		'supports'    => [ 'title', 'editor', 'excerpt' ],
		'has_archive' => false,
		'public'      => true,
		'labels'      => [
			'name'          => 'Positions',
			'add_new_item'  => 'Add New Position',
			'edit_item'     => 'Edit Position',
			'all_items'     => 'All Positions',
			'singular_name' => 'Position',
		],
		'menu_icon'   => 'dashicons-id-alt',
	] );
}

add_action( 'init', 'abacus_post_types' );

/**
 * Redirect to Home Page if Empty Cart
 */
add_action( 'template_redirect', 'redirectToMainPage' );
function redirectToMainPage() {
	global $woocommerce;
	
	if( is_checkout() && 0 == sprintf( _n( '%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes' ),
			$woocommerce->cart->cart_contents_count ) && ! isset( $_GET['key'] ) ) {
		wp_redirect( home_url() );
		exit;
	}
}

/**
 * Get all ID Post
 *
 * @return array
 */
function getAllTeamByTerm( $term_id ): array {
	$allPosts = [];
	
	$arg = [
		'post_type'      => 'teams',
		'posts_per_page' => - 1,
		'tax_query'      => [
			[
				'taxonomy' => 'position',
				'field'    => 'id',
				'terms'    => $term_id,
			],
		],
	];
	
	$query = new WP_Query( $arg );
	if( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$allPosts[] = get_the_ID();
		}
	} else {
		return [];
	}
	
	return $allPosts;
	
}