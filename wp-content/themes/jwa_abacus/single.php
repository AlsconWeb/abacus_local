<?php

/*
* Created 11.03.20
* Version 1.0.0
* Last update 11.03.20
* Author: Maxym Y
*/

?>
<?php get_header();?>
<section>
  <?php while ( have_posts() ) : the_post(); ?>
    <?php $id = get_the_ID(); ?>
    <div class="blogs inner-blog">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
            <div class="content">
              <div class="breadcrumbs">
                <span>
                  <a href="<?php echo get_home_url(); ?>">
                    <span><?php _e('Home', 'jwa_abacus'); ?></span>
                  </a>
                </span>
                <span>
                  <a href="<?php echo get_permalink( 80 ) ?>">
                    <span><?php _e('News', 'jwa_abacus'); ?></span>
                  </a>
                </span>
                <span>
                  <?php echo get_the_title(); ?>
                </span>
              </div>
              <h1><?php echo get_the_title(); ?></h1>
              <ul class="meta dfr">
                <!-- <li><?php echo get_the_author($post); ?></li> -->
                <li><?php echo get_the_date('F d, Y', $post); ?></li>
              </ul>
              <img src="<?php echo get_the_post_thumbnail_url( $post, 'full' ); ?>" alt="">
              <?php the_content(); ?>
            </div>
            <div class="sidebar">
              <?php
              if ( function_exists('dynamic_sidebar') )
                dynamic_sidebar('news-sidebar');
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="blog">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2><?php _e('Other News', 'jwa_abacus'); ?></h2>
            <div class="items dfr">
              <?php
              $posts = get_posts( array(
              	'numberposts' => 3,
              	'category'    => 0,
              	'orderby'     => 'date',
              	'order'       => 'DESC',
              	'include'     => array(),
              	'exclude'     => array($id),
              	'meta_key'    => '',
              	'meta_value'  =>'',
              	'post_type'   => 'post',
              	'suppress_filters' => true,
              ) );

              foreach( $posts as $post ):
              	setup_postdata($post);
                $id = $post->ID;
                ?>
                <a class="item" href="<?php echo get_permalink($id); ?>">
                  <img src="<?php echo get_the_post_thumbnail_url( $post, 'blog_thumbnail_main' ); ?>" alt="#">
                  <h4><?php echo $post->post_title; ?></h4>
                  <ul class="meta dfr">
                    <!-- <li class="author">
                      <?php _e('by', 'jwa_abacus'); ?>
                      <?php echo get_the_author($post); ?>
                    </li> -->
                    <li class="date">
                      <?php echo get_the_date('F d, Y g:iA', $post); ?>
                    </li>
                  </ul>
                  <p><?php echo abacus_excerpt(195, $post); ?></p>
                </a>
                <?php
              endforeach;

              wp_reset_postdata();
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>


  <?php endwhile; ?>
</section>
<?php get_footer();?>
