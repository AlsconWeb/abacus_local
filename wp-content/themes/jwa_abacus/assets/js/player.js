jQuery(function ($) {
	if($('.podcast-list').length){
		'use strict'
		var supportsAudio = !!document.createElement('audio').canPlayType;
		if (supportsAudio) {
			// initialize plyr
			var player = new Plyr('#audio1', {
				controls: [
					'restart',
					'play',
					'progress',
					'current-time',
					'duration',
					'mute',
					'volume',
					'download'
				]
			});
			// initialize playlist and controls
			var index = 0,
				playing = false,
				//mediaPath = 'https://archive.org/download/mythium/',
				mediaPath = '',
				extension = '',
				tracks = sound_tracks,
				// tracks = [{
				// 	"track": 1,
				// 	"name": "All This Is - Joe L.'s Studio",
				// 	"duration": "2:46",
				// 	"file": "JLS_ATI",
				// 	"img":"http://switch.justwebagency.com/abacus/img/abacus.jpg"
				// }, {
				// 	"track": 2,
				// 	"name": "The Forsaken (Take 2) - Smith St. Basement (Nov. '03)",
				// 	"duration": "8:36",
				// 	"file": "SSB___11_03_TFTake_2",
				// 	"img":"http://switch.justwebagency.com/abacus/img/abacus.jpg"
				// }],
				buildPlaylist = $.each(tracks, function (key, value) {
					var trackNumber = value.track,
						trackName = value.name,
						trackDuration = value.duration,
						trackImg = value.img;
					if (trackNumber.toString().length === 1) {
						trackNumber = '0' + trackNumber;
					}
					$('.podcast-list').append('<li> \
						<div class="plItem dfr"> \
							<img src="'+ trackImg +'" alt="'+ trackName +'"> \
							<div class="description dfr"> \
								<p class="plTitle">' + trackName + '</p> \
								<p class="plLength">' + trackDuration + '</p> \
							</div> \
						</div> \
					</li>');
				}),
				trackCount = tracks.length,
				npAction = $('#npAction'),
				npTitle = $('#npTitle'),
				npImg = '',
				audio = $('#audio1').on('play', function () {
					playing = true;
					npAction.text('Now Playing...');
				}).on('pause', function () {
					playing = false;
					npAction.text('Paused...');
				}).on('ended', function () {
					npAction.text('Paused...');
					if ((index + 1) < trackCount) {
						index++;
						loadTrack(index);
						audio.play();
					} else {
						audio.pause();
						index = 0;
						loadTrack(index);
					}
				}).get(0),
				btnPrev = $('#btnPrev').on('click', function () {
					if ((index - 1) > -1) {
						index--;
						loadTrack(index);
						if (playing) {
							audio.play();
						}
					} else {
						audio.pause();
						index = 0;
						loadTrack(index);
					}
				}),
				btnNext = $('#btnNext').on('click', function () {
					if ((index + 1) < trackCount) {
						index++;
						loadTrack(index);
						if (playing) {
							audio.play();
						}
					} else {
						audio.pause();
						index = 0;
						loadTrack(index);
					}
				}),
				li = $('.podcast-list li').on('click', function () {
					var id = parseInt($(this).index());
					if (id !== index) {
						playTrack(id);
					}
				}),
				loadTrack = function (id) {
					$('.active').removeClass('active');
					$('.podcast-list li:eq(' + id + ')').addClass('active');
					npTitle.text(tracks[id].name);
					$('#npTitle2').html(tracks[id].name);
					//console.log(tracks[id].name);
	//				if($($('#nowPla img').length)){
	//
	//				}else{
						$('#nowPlay img').remove();
						$('#nowPlay').prepend($('.podcast-list li:eq(' + id + ') img').clone())
	//				}
					index = id;
					audio.src = mediaPath + tracks[id].file + extension;
					updateDownload(id, audio.src);
				},
				updateDownload = function (id, source) {
					player.on('loadedmetadata', function () {
						$('a[data-plyr="download"]').attr('href', source);
					});
				},
				playTrack = function (id) {
					loadTrack(id);
					audio.play();
				};
			extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
			loadTrack(index);
		} else {
			// no audio support
			$('.column').addClass('hidden');
			var noSupport = $('#audio1').text();
			$('.container').append('<p class="no-support">' + noSupport + '</p>');
		}
	}
});
