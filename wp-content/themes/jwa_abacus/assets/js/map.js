if($('#map').length){
	google.maps.event.addDomListener(window, 'load', init);
	var mapCoordinatesLat = $('#map').data('lat'),
		mapCoordinatesLng = $('#map').data('lng');

	function init() {
		var mapOptions = {
			zoom: 11,
			center: new google.maps.LatLng(+mapCoordinatesLat,+mapCoordinatesLng),
			styles: [{
				"featureType": "landscape.man_made",
				"elementType": "geometry",
				"stylers": [{
					"color": "#f7f1df"
				}]
			}, {
				"featureType": "landscape.natural",
				"elementType": "geometry",
				"stylers": [{
					"color": "#d0e3b4"
				}]
			}, {
				"featureType": "landscape.natural.terrain",
				"elementType": "geometry",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "poi",
				"elementType": "labels",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "poi.business",
				"elementType": "all",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "poi.medical",
				"elementType": "geometry",
				"stylers": [{
					"color": "#fbd3da"
				}]
			}, {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [{
					"color": "#bde6ab"
				}]
			}, {
				"featureType": "road",
				"elementType": "geometry.stroke",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "road",
				"elementType": "labels",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "#F0BC5E"
				}]
			}, {
				"featureType": "road.highway",
				"elementType": "geometry.stroke",
				"stylers": [{
					"color": "#efd151"
				}]
			}, {
				"featureType": "road.arterial",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "#ffffff"
				}]
			}, {
				"featureType": "road.local",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "black"
				}]
			}, {
				"featureType": "transit.station.airport",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "#cfb2db"
				}]
			}, {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [{
					"color": "#a2daf2"
				}]
			}]
		};
		var mapElement = document.getElementById('map');
		var map = new google.maps.Map(mapElement, mapOptions);
		var contentString = '<div id="content">'+
      '<ul class="infowindow">'+
        '<li>'+
        '<strong>Office Address</strong> 287 MacPherson Ave.<br>Suite #202<br>Toronto, Ontario<br>M4V 1A4</li>'+

        '<li>'+
				'<strong>Contact Us</strong>'+
				'<a class="icon-phone" href="tel:416-440-1600">416-440-1600</a>'+
        '<a class="icon-fax" href="fax:416-946-1296">416-946-1296</a>'+

				'<div class="hide_mobile">Monday to Friday<br>9:00 AM – 5:30 PM</div></li>'+
                '</ul>'+
      '</div>';
		var infowindow = new google.maps.InfoWindow({
	    content: contentString
	  });
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(+mapCoordinatesLat, +mapCoordinatesLng),
			map: map,
			icon:'/wp-content/themes/jwa_abacus/assets/icons/marker.svg?v=2'
		});
		infowindow.open(map, marker);
		marker.addListener('click', function() {
	    infowindow.open(map, marker);
	  });
	}
}
