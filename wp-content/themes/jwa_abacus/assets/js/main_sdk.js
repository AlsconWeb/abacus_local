jQuery(document).ready(($) => {
	let errorObj = $(".alert.alert-danger");
	
	$(".invoice_form").submit(function (e) {
		e.preventDefault();
		let data = {
			action: "get_invoice",
			query: "search",
			uid: $("input[name=user_id]").val(),
			invid: $("input[name=invoice_id]").val(),
			nonce: $("input[name=nonce]").val(),
		}
		
		if (data.invid.length == 0) {
			$(errorObj).append("<p>Enter Invoice ID</p>");
			$(errorObj).css("opacity", 1);
		} else {
			$(errorObj).css("opacity", 0);
			errorObj.find("p.error").remove();
		}
		$.ajax({
			type: "POST",
			url: wc_add_to_cart_params.ajax_url,
			data: data,
			beforeSend: () => {
				$('.preloader').show();
				$('.preloader').delay(3500).fadeOut(600);
			},
			success: function (res) {
				console.log(res)
				if (res.success) {
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'Thank you, the invoice has been found.  Please click the button below to proceed with the payment.',
						showCancelButton: true,
						confirmButtonText: 'Proceed with Payment'
					}).then((result) => {
						if (result.value) {
							location.href = 'https://abacusgroup.ca/cart/?add-to-cart=' + res.data.prodID;
						}
					})
					// $(".go-to-pay .cart-btn").remove();
					// $(".go-to-pay").append('<a class="button cart-btn" href="https://abacusgroup.ca/cart/?add-to-cart=' + res.data.prodID + '">Proceed with Payment</a>');
				} else {
					console.log(res.data.response);
					Swal.fire({
						icon: 'error',
						title: 'Oops... Error',
						html: '<p>' + res.data.code + '</p><p>' + res.data.message + '</p>',
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("error...", xhr);
			},
		});
	});
	
	
	$(".alert-danger .close").click(function (e) {
		e.preventDefault();
		$(errorObj).css("opacity", 0);
	});
	
});