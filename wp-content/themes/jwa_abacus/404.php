<?php

/*
* Created 11.03.20
* Version 1.0.0
* Last update 11.03.20
* Author: Maxym Y
*/

?>
<?php get_header();?>
<section>
  <div class="container">
    <div class="page-404">
      <h1>404<span>Oops. Sorry We Cant Find The Page!</span></h1>
      <p>Either something went wrong or the page doesnt exist anymore</p>
      <a class="button" href="<?php echo get_home_url(); ?>">Home</a>
    </div>
  </div>
</section>
<?php get_footer();?>
