<?php

/*
* Created 11.03.20
* Version 1.0.0
* Last update 11.03.20
* Author: Maxym Y
*/
?>
<?php if ( get_field( 'hide_footer' )[0] != 'Yes' and ! is_404() ): ?>
	<?php $map = get_field( 'map', 'option' ); ?>
	<div class="map">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfc">
					<?php
					$form_id = get_field( 'form', 'option' );
					echo do_shortcode( '[contact-form-7 id="' . $form_id[0] . '"]' );
					?>
				</div>
			</div>
		</div>
		<div id="map" data-lat="<?php echo $map['lat'] ?>" data-lng="<?php echo $map['lng'] ?>"></div>
	</div>
<?php endif; ?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
				<a class="logo" href="<?php echo get_home_url(); ?>">
					<img src="<?php the_field( 'footer_logo', 'option' ) ?>" alt="#">
				</a>
				<?php
				wp_nav_menu( [
					'theme_location' => 'footer_menu',
					'container'      => false,
				] );
				?>
				<?php
				$social_links = get_field( 'social_links', 'option' );
				?>
				<ul class="soc">
					<?php foreach ( $social_links as $link ): ?>
						<li class="<?php echo $link['icon']; ?>">
							<a target="_blank" href="<?php echo $link['link']; ?>"></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<p class="copyright"><?php the_field( 'copyright', 'option' ) ?></p>
				<p class="copyright dev"><a href="https://www.justwebagency.com/">Developed by Just Web Agency</a></p>
			</div>
		</div>
	</div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5media/1.1.8/html5media.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.5.6/plyr.min.js"></script>
<link rel="stylesheet" href="https://cdn.plyr.io/3.5.6/plyr.css">
</body>
<?php wp_footer(); ?>
</html>
