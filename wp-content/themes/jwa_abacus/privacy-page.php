<?php

/* Template Name: Privacy template */

/*
* Created 11.03.20
* Version 1.0.0
* Last update 11.03.20
* Author: Maxym Y
*/

?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <div class="blogs inner-blog privacy">
    <div class="container">
      <h1><?php echo get_the_title(); ?></h1>
        <div class="content">
          <?php the_content(); ?>
        </div>
    </div>
  </div>
<?php endwhile; ?>
<?php get_footer(); ?>
