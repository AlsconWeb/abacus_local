<?php
/**
 * Created 15.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * Template Name: Form invoice
 */

get_header();

?>
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 map">
				<div class='preloader'></div>
				<h1 style="font-size: 40px;"><?php the_title(); ?></h1>
				<p> Thank you for using our services.</p>
				<p>Please type the invoice number you received by email below and click search.</p>
				<div class="alert alert-danger" style="opacity: 0">
					<p class="error"></p>
					<i class="glyphicon glyphicon-remove close">x</i>
				</div>
				<form action="" method="post" class="invoice_form form" style="margin-bottom: 20px;">
					<div class="row">
						<div class="col-lg-6">
							<div class="input-group">
								<input type="text" class="form-control" name="invoice_id" placeholder="Invoice ID">
							</div><!-- /input-group -->
						</div><!-- /.col-lg-6 -->
					</div><!-- /.row -->
					<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'invoice' ) ?>">
					<input type="submit" class="button" value="Search">
				</form>
				<div class="go-to-pay">
				
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer( 'styleTwo' ); ?>
