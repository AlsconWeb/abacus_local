<?php

/*
* Created 11.03.20
* Version 1.0.1
* Last update 28.03.20
* Author: Alex L
*/

?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; ?>

<?php
if ( is_checkout() ) {
	get_footer( 'styleTwo' );
} else {
	get_footer();
}
?>
