<?php

/*
* Created 11.03.20
* Version 1.0.0
* Last update 11.03.20
* Author: Maxym Y
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!--(if lt IE 9)-->
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" async></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" async></script>
	<!--(endif)-->
	<?php wp_head(); ?>
</head>
<?php if ( is_front_page() ): ?>
<body <?php body_class(); ?>>
<?php else: ?>
<body <?php body_class( 'inner-page' ); ?>>
<?php endif; ?>
<div id="head_of_page">
</div>
<a class="top_link" href="#head_of_page">
	<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/arrow-up.svg" alt="">
</a>
<div id="preloader_jwa"
     style="z-index:99999;background:white;position:fixed;width:100%;height:100vh;top:0px;left:0px;"></div>
<header>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
				<a class="logo" href="<?php echo get_home_url(); ?>">
					<img src="<?php the_field( 'header_logo', 'option' ); ?>" alt="Logo">
				</a>
				<div class="burger-menu"><span></span><span></span><span></span></div>
				<?php
				wp_nav_menu( [
					'theme_location' => 'top_menu',
					'container'      => false,
				] );
				?>
				<form class="search" method="get">
					<input name="s" type="search">
					<button class="icon-search dfc"></button>
				</form>
			</div>
		</div>
	</div>
</header>
