<?php
/**
 * Created 28.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */
?>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
				<a class="logo" href="<?php echo get_home_url(); ?>">
					<img src="<?php the_field( 'footer_logo', 'option' ) ?>" alt="#">
				</a>
				<?php
				wp_nav_menu( [
					'theme_location' => 'footer_menu',
					'container'      => false,
				] );
				?>
				<?php
				$social_links = get_field( 'social_links', 'option' );
				?>
				<ul class="soc">
					<?php foreach ( $social_links as $link ): ?>
						<li class="<?php echo $link['icon']; ?>">
							<a target="_blank" href="<?php echo $link['link']; ?>"></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<p class="copyright"><?php the_field( 'copyright', 'option' ) ?></p>
			</div>
		</div>
	</div>
</footer>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5media/1.1.8/html5media.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.5.6/plyr.min.js"></script>
<link rel="stylesheet" href="https://cdn.plyr.io/3.5.6/plyr.css">
</body>
<?php wp_footer(); ?>
</html>
