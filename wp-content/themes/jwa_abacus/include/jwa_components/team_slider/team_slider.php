<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Team Slider
class vcTeamSlider extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_team_slider' ), 12);
        add_shortcode( 'jwa_team_slider', array( $this, 'vc_team_slider_html' ) );
    }

    // Element Mapping
    public function vc_team_slider() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        $posts = get_posts( array(
          'numberposts' => -1,
          'category'    => 0,
          'orderby'     => 'date',
          'order'       => 'DESC',
          'include'     => array(),
          'exclude'     => array(),
          'meta_key'    => '',
          'meta_value'  =>'',
          'post_type'   => 'teams',
          'suppress_filters' => true,
        ) );
        $select_items = array();
        foreach( $posts as $post ):
          $select_items[$post->post_title . '-' . $post->ID] = $post->ID;
        endforeach;
        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Team Slider', 'jwa_abacus'),
                'base' => 'jwa_team_slider',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'heading' => __( 'Title', 'jwa_abacus' ),
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'dropdown_multi',
                        'heading' => __( 'Select members',  'jwa_abacus' ),
                        'param_name' => 'members',
                        'value' => $select_items,
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_team_slider_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'members'   => '',
                ),
                $atts
            )
        );
        //$team = vc_param_group_parse_atts($team);
				ob_start();
        $members = explode(",", $members);
        $class = '';
        ?>
        <!-- team_slider -->
        <?php if (!$title):
          $class = 'team_second';
        endif; ?>
        <div class="team <?php echo $class; ?>">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
                <?php if ($title): ?>
                  <h2><?php echo $title; ?></h2>
                <?php endif; ?>

                <div class="team-slider">
                  <?php foreach ($members as $member): ?>
                    <div class="item">
                     <div class="img">
						<img src="<?php the_field('photo', $member) ?>" alt="#">
                     </div>
                     <div class="desc">
						  <h5><?php echo get_the_title($member) ?><span><?php the_field('position', $member) ?></span></h5>
						  <ul class="dfr">
							<?php if(get_field('speciality2', $member))
							  echo '<li>' . get_field('speciality2', $member) . '</li>';
							 ?>
							<?php $speciality = get_field('speciality', $member); ?>
							<?php for ($i=0; $i < count($speciality); $i++) {
							  $separator = ',';
							  if($i == count($speciality)-1) {
								$separator = '';
							  }
							  ?>
							  <li><?php echo $speciality[$i] ?><?php echo $separator; ?></li>
							  <?php
							}
							?>
						  </ul>
						  <?php the_field('excerpt', $member); ?>
						  <!-- <a class="button" href="<?php echo get_permalink($member); ?>"><?php _e('Read More', 'jwa_abacus'); ?></a> -->
                     	
                     </div>
                    </div>
                  <?php endforeach; ?>
                </div>
                <div class="team-photo">
                <?php foreach ($members as $member): ?>
                  <div class="item"><img src="<?php the_field('photo', $member) ?>" alt="#"></div>
                <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcTeamSlider();
