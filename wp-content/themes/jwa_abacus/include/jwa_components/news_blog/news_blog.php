<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// News Blog
class vcNewsBlog_jwa extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_news_blog' ), 12);
        add_shortcode( 'jwa_news_blog', array( $this, 'vc_news_blog_html' ) );
    }

    // Element Mapping
    public function vc_news_blog() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        $posts = get_posts( array(
          'numberposts' => -1,
          'category'    => 0,
          'orderby'     => 'date',
          'order'       => 'DESC',
          'include'     => array(),
          'exclude'     => array(),
          'meta_key'    => '',
          'meta_value'  =>'',
          'post_type'   => 'post',
          'suppress_filters' => true,
        ) );
        $select_items = array();
        foreach( $posts as $post ):
          $select_items[$post->post_title . '-' . $post->ID] = $post->ID;
        endforeach;
        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('News Blog', 'jwa_abacus'),
                'base' => 'jwa_news_blog',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'dropdown_multi',
                        'heading' => __( 'Select 2 posts for head of archive',  'jwa_abacus' ),
                        'param_name' => 'top_posts',
                        'value' => $select_items,
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_news_blog_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'top_posts'   => '',
                ),
                $atts
            )
        );
        //$team = vc_param_group_parse_atts($team);
				ob_start();
        $top_posts = explode(",", $top_posts);
        ?>
        <div class="blogs">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
                <h1><?php echo get_the_title(); ?></h1>
                <div  class="first-posts dfr">
                  <?php
                  for ($i=0; $i < 2; $i++):
                    if($top_posts[$i] == null):
                      break;
                    endif;
                    $size = '';
                    if ($i == 0) {
                      $size = 'blog_thumbnail_big_first';
                    } else {
                      $size = 'blog_thumbnail_big_second';
                      $size = 'blog_thumbnail_big_first';
                    }
                    ?>
                    <a style="display:none;" class="post dfc" href="<?php echo get_permalink($top_posts[$i]); ?>">
                      <img src="<?php echo get_the_post_thumbnail_url( $top_posts[$i], $size ); ?>" alt="">
                      <div class="description">
                        <h3>
                          <?php echo get_the_title($top_posts[$i]) ?>
                        </h3>
                        <div class="meta dfr">
                          <!-- <p><?php _e('by', 'jwa_abacus'); ?> <?php echo get_the_author($top_posts[$i]); ?></p> -->
                          <p><?php echo get_the_date('F d, Y', $top_posts[$i]); ?></p>
                        </div>
                      </div>
                    </a>
                    <?php
                  endfor;
                  ?>
                </div>
                <div class="content">
                  <div class="news dfr">
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                      'post_type' => 'post',
                    	'posts_per_page' => 10,
                    	//'paged'  => true,
                      'paged'  => $paged,
                      'post_status' => 'publish',
                    );

                    $query = new WP_Query( $args );
                    $i = 1;
                    if ( $query->have_posts() ) :
                    	while ( $query->have_posts() ) :
                    		$query->the_post();
                        $id = get_the_ID();
                        $post = get_post($id);
                        $char_count = 400;
                        $size = 'blog_thumbnail_small_second';
                        $class = 'dfr';
                        $excerpt_class = '';
                        if ($i == 1) {
                          $char_count = 230;
                          $size = 'blog_thumbnail_big_second';
                          $class = 'dfc';
                        }
                        if ( ($i >= 2) and ($i <= 5)) {
                          $char_count = 230;
                          $size = 'blog_thumbnail_small_first';
                          $size = 'blog_thumbnail_big_second';
                          $class .= ' right_block_posts';
                          $excerpt_class = 'hide_desktop';
                        }
                        ?>
                            <a style="" class="post <?php echo $class ?>" href="<?php echo get_permalink($id) ?>">
                              <img src="<?php echo get_the_post_thumbnail_url( $post, $size ); ?>" alt="">
                              <div class="description">
                                <h3>
                                  <?php echo $post->post_title; ?>
                                </h3>
                                <div class="meta dfr">
                                  <!-- <p><?php _e('by', 'jwa_abacus'); ?> <?php echo get_the_author($post); ?></p> -->
                                  <p><?php echo get_the_date('F d, Y', $post); ?></p>
                                </div>
                                <p class="<?php echo $excerpt_class ?>"><?php echo abacus_excerpt($char_count, $post); ?></p>
                              </div>
                            </a>
                          <?php
                        $i++;
                      endwhile;
                    endif;
                    ?>
                  </div>

                  <nav class="navigation pagination" role="navigation">
                    <?php
                    if (function_exists('wp_pagenavi')) {
                      wp_pagenavi( array( 'query' => $query ) );
                    }
                    ?>
                  </nav>
                </div>
                <div class="sidebar">
                  <?php
                  if ( function_exists('dynamic_sidebar') )
                		dynamic_sidebar('news-sidebar');
                	?>
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcNewsBlog_jwa();
