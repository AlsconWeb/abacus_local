<?php
/**
 * Created 01.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 */

/**
 * Class IWPTeamBanner
 */
class IWPTeamBanner extends WPBakeryShortCode {
	
	/**
	 * IWPTeamBanner constructor.
	 */
	function __construct() {
		add_action( 'init', [ $this, 'map' ], 12 );
		add_shortcode( 'iwp_team_banner', [ $this, 'outputHtml' ] );
	}
	
	/**
	 * Map elements
	 *
	 * @throws Exception
	 */
	public function map() {
		
		// Stop all if VC is not enabled
		if( ! defined( 'WPB_VC_VERSION' ) ) {
			return;
		}
		
		// Map the block with vc_map()
		vc_map(
			[
				'name'     => __( 'Team Banner', 'jwa_abacus' ),
				'base'     => 'iwp_team_banner',
				'category' => __( 'JWA', 'jwa_abacus' ),
				'params'   => [
					[
						'type'       => 'attach_image',
						'heading'    => __( 'Image', 'jwa_abacus' ),
						'param_name' => 'image',
						'value'      => '',
					],
				
				],
			]
		);
		
	}
	
	
	/**
	 * Output HTML
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public function outputHtml( $atts, $content ) {
		
		ob_start();
		?>
		<div class="banner-team">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h1 class="title"><?php the_title(); ?></h1>
						<div class="img">
							<img src="<?php echo wp_get_attachment_image_url( $atts['image'], 'full' ); ?>"
							     alt="<?php echo get_the_title( $atts['image'] ); ?>">
							<i class="icon-swipe"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php
		return ob_get_clean();
		
	}
	
}

new IWPTeamBanner();