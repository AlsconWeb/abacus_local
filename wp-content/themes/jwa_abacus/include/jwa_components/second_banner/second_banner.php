<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Second Banner
class vcSecondBanner extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_second_banner' ), 12);
        add_shortcode( 'jwa_second_banner', array( $this, 'vc_second_banner_html' ) );
    }

    // Element Mapping
    public function vc_second_banner() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Second Banner', 'jwa_abacus'),
                'base' => 'jwa_second_banner',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Main Heading', 'jwa_abacus' ),
                        'param_name' => 'main_heading',
                        'admin_label' => true,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Content', 'jwa_abacus' ),
                        'param_name' => 'content',
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'attach_image',
                        'heading' => __( 'Image', 'jwa_abacus' ),
                        'param_name' => 'image',
                        'value' => '',
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_second_banner_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'main_heading'   => '',
                    //'content'       => '',
                    'image'         => '',
                ),
                $atts
            )
        );

				ob_start();
        ?>
        <div class="banner">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
                <div class="left-block">
                  <h1><?php echo $main_heading; ?></h1>
                  <p>
                    <?php echo $content; ?>
                  </p>
                </div>
                <div class="right-block">
                  <img src="<?php echo wp_get_attachment_image_url( $image, 'full' ); ?>" alt="#">
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcSecondBanner();
