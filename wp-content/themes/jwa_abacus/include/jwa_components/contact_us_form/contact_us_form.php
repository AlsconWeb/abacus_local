<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Contact us Form
class vcContactUsForm extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_contact_us_form' ), 12);
        add_shortcode( 'jwa_contact_us_form', array( $this, 'vc_contact_us_form_html' ) );
    }

    // Element Mapping
    public function vc_contact_us_form() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        $forms = get_posts( array(
        	'numberposts' => -1,
        	'category'    => 0,
        	'orderby'     => 'date',
        	'order'       => 'DESC',
        	'include'     => array(),
        	'exclude'     => array(),
        	'meta_key'    => '',
        	'meta_value'  =>'',
        	'post_type'   => 'wpcf7_contact_form',
        	'suppress_filters' => true,
        ) );
        $select_items = array();
        foreach ($forms as $form) {
          $select_items[$form->post_title] = $form->ID;
        }
        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Contact us Form', 'jwa_abacus'),
                'base' => 'jwa_contact_us_form',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Location', 'jwa_abacus' ),
                        'param_name' => 'location',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Phone', 'jwa_abacus' ),
                        'param_name' => 'phone',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Fax', 'jwa_abacus' ),
                        'param_name' => 'fax',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Hours', 'jwa_abacus' ),
                        'param_name' => 'hours',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Select form',  'gsklaw' ),
                        'param_name' => 'form',
                        'value' => $select_items,
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_contact_us_form_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'form'         => '',
                    'phone'        => '',
                    'fax'          => '',
                ),
                $atts
            )
        );

				ob_start();
        ?>
        <?php $map = get_field('map', 'option'); ?>
        <div class="map contacts">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo do_shortcode( '[contact-form-7 id="' . $form . '"]' ) ?>
                <ul>
                  <li>
                    <strong><?php _e('Office Address', 'jwa_abacus'); ?></strong>
                    <?php echo $atts['location']; ?>
                  </li>
                  <li>
                    <a class="icon-phone" href="tel:<?php echo $phone ?>"><?php echo $phone ?></a>
                    <a class="icon-fax" href="fax:<?php echo $fax ?>"><?php echo $fax ?></a>
                  </li>
                  <li>
                    <strong><?php _e('Hours of Operation', 'jwa_abacus'); ?></strong>
                    <?php echo $atts['hours']; ?>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div id="map" data-lat="<?php echo $map['lat'] ?>" data-lng="<?php echo $map['lng'] ?>"></div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcContactUsForm();
