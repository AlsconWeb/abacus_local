<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Latest Blog
class vcLatestBlog extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_latest_blog' ), 12);
        add_shortcode( 'jwa_latest_blog', array( $this, 'vc_latest_blog_html' ) );
    }

    // Element Mapping
    public function vc_latest_blog() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Latest Blog', 'jwa_abacus'),
                'base' => 'jwa_latest_blog',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Title', 'jwa_abacus' ),
                        'param_name' => 'title',
                        'admin_label' => true,
                        'weight' => 0,
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_latest_blog_html( $atts ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                ),
                $atts
            )
        );

				ob_start();
        ?>

        <div class="blog">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $title ?></h2>
                <div class="items dfr">
                  <?php
                  $posts = get_posts( array(
                  	'numberposts' => 3,
                  	'category'    => 0,
                  	'orderby'     => 'date',
                  	'order'       => 'DESC',
                  	'include'     => array(),
                  	'exclude'     => array(),
                  	'meta_key'    => '',
                  	'meta_value'  =>'',
                  	'post_type'   => 'post',
                  	'suppress_filters' => true,
                  ) );

                  foreach( $posts as $post ):
                  	setup_postdata($post);
                    $id = $post->ID; ?>
                      <a class="item" href="<?php echo get_permalink($id); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url( $post, 'blog_thumbnail_2' ); ?>" alt="#">
                        <h4><?php echo $post->post_title; ?></h4>
                        <ul class="meta dfr">
                          <!-- <li class="author">
                            <?php _e('by', 'jwa_abacus'); ?>
                            <?php echo get_the_author($post); ?>
                          </li> -->
                          <li class="date">
                            <?php echo get_the_date('F d, Y', $post); ?>
                          </li>
                        </ul>
                        <p><?php echo abacus_excerpt(195, $post); ?></p>
                      </a>
                    <?php
                  endforeach;
                  wp_reset_postdata();
                  ?>
                </div>
                <a class="button" style="margin-left: auto; margin-right: auto;" href="https://abacusgroup.ca/blog/">Read More</a>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcLatestBlog();
