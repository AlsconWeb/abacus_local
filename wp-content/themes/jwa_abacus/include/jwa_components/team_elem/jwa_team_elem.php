<?php
/**
 * Created 07.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

class JWA_team_elem {
	public function __construct() {
		// Registers the shortcode in WordPress
		add_shortcode( 'jwa_team_elem', [ $this, 'output' ] );
		
		// Map shortcode to Visual Composer
		if( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'jwa_team_elem', [ $this, 'map' ] );
		}
	}
	
	public static function map() {
		
		return [
			'name'                    => esc_html__( 'Team', 'jwa' ),
			'description'             => esc_html__( 'Add new Team', 'jwa' ),
			'base'                    => 'jwa_team_elem',
			'category'                => __( 'JWA', 'jwa' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'heading'     => __( 'Specifications', 'jwa' ),
					'param_name'  => 'team_elem',
					'value'       => '',
					'params'      => [
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Image', 'jwa' ),
							'param_name' => 'image',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Name', 'jwa' ),
							'param_name' => 'name',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Position', 'jwa' ),
							'param_name' => 'position',
						],
						[
							'type'       => 'param_group',
							'value'      => '',
							'heading'    => __( 'Certificates', 'jwa' ),
							'param_name' => 'certificate',
							'params'     => [
								[
									'type'       => 'textfield',
									'value'      => '',
									'heading'    => __( 'Name', 'jwa' ),
									'param_name' => 'certificate_item',
								],
							],
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Description', 'jwa' ),
							'param_name' => 'description',
						],
					],
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				// Custom css
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'jwa' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'jwa' ),
				],
			],
		];
	}
	
	public static function output( $atts, $content = null ) {
		ob_start();
		
		include get_stylesheet_directory() . '/include/wpbkComponents/template/jwa_team_elem.php';
		
		return ob_get_clean();
	}
}

new JWA_team_elem();