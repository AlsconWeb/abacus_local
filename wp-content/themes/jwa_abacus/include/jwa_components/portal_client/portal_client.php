<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Portal Client
class vcPortalClient extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_portal_client' ), 12);
        add_shortcode( 'jwa_portal_client', array( $this, 'vc_portal_client_html' ) );
    }

    // Element Mapping
    public function vc_portal_client() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Portal Client', 'jwa_abacus'),
                'base' => 'jwa_portal_client',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Main Heading', 'jwa_abacus' ),
                        'param_name' => 'main_heading',
                        'admin_label' => true,
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textarea_html',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Content', 'jwa_abacus' ),
                        'param_name' => 'content',
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'YouTube title', 'jwa_abacus' ),
                        'param_name' => 'youtube_link',
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Button title', 'jwa_abacus' ),
                        'param_name' => 'button_title',
                        'weight' => 0,
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Button link', 'jwa_abacus' ),
                        'param_name' => 'button_link',
                        'weight' => 0,
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_portal_client_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'main_heading'   => '',
                    'button_title'       => '',
                    'button_link'       => '',
                    'youtube_link'  => '',
                    'image'         => '',
                ),
                $atts
            )
        );

				ob_start();
        ?>
        <div class="portal_client">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr flex_container">
                <div class="left-block">
                  <h1><?php echo $main_heading; ?></h1>
                  <p>
                    <?php echo $content; ?>
                  </p>
                  <a class="portal_button" target="_blank" href="<?php echo $button_link ?>"><?php echo $button_title ?></a>
                </div>
                <div class="right-block">
                  <?php
                  $youtube_link = preg_replace(
                    "/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
                    "<iframe class=\"youtube_iframe\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",
                    $youtube_link);
                  ?>
                  <div class="youtube_wrapper">
                    <?php echo $youtube_link ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcPortalClient();
