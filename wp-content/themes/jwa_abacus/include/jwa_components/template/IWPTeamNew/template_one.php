<?php
/**
 * Created 01.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */


$arg = [
	'post_type'      => 'teams',
	'posts_per_page' => $atts['count'],
	'tax_query'      => [
		[
			'taxonomy' => 'position',
			'field'    => 'id',
			'terms'    => $atts['category'],
		],
	],
];

$query = new WP_Query( $arg );
?>


<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2><?php echo $atts['title'] ?? ''; ?></h2>
			<div class="partners">
				<?php if( $query->have_posts() ): ?>
					<?php while ( $query->have_posts() ):$query->the_post();
						$id = get_the_ID(); ?>
						<div class="partner">
							<div class="img">
								<?php if( has_post_thumbnail( $id ) ): the_post_thumbnail( 'team_photo' ); ?>
								<?php else: ?>
									<img src="<?php echo get_field( 'photo', $id ); ?>" alt="#">
								<?php endif; ?>
							</div>
							<h3><?php the_title(); ?></h3>
							<?php $termName = wp_get_post_terms( $id, 'position' )[0]->name; ?>
							<h4><?php echo $termName ?></h4>
							<a class="link" href="<?php the_permalink(); ?>"></a>
						</div>
					<?php endwhile;
					wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

