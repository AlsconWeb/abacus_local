<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Our Mission
class vcOurMission extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_our_mission' ), 12);
        add_shortcode( 'jwa_our_mission', array( $this, 'vc_our_mission_html' ) );
    }

    // Element Mapping
    public function vc_our_mission() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Our Mission', 'jwa_abacus'),
                'base' => 'jwa_our_mission',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'heading' => __( 'Title', 'jwa_abacus' ),
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                      'heading' => 'Mission items',
                      'value' => '',
                      'type'  => 'param_group',
                      'param_name' => 'items',
                      'params' => array(
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Title', 'jwa_abacus' ),
                            'param_name' => 'title',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textarea',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Description', 'jwa_abacus' ),
                            'param_name' => 'description',
                        ),

                      )
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_our_mission_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'items'   => '',
                ),
                $atts
            )
        );
        //$team = vc_param_group_parse_atts($team);
				ob_start();
        $items = vc_param_group_parse_atts($items);
        ?>
        <div class="mission">
  				<div class="container">
  					<div class="row">
  						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  							<h2><?php echo $title ?></h2>
  							<div class="items">
                  <?php foreach ($items as $item): ?>
  								<div class="item icon-checked">
  									<h3><?php echo $item['title']; ?></h3>
  									<p><?php echo $item['description']; ?></p>
  								</div>
                  <?php endforeach; ?>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcOurMission();
