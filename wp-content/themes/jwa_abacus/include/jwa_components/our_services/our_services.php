<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Our Services
class vcOurServices extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_our_services' ), 12);
        add_shortcode( 'jwa_our_services', array( $this, 'vc_our_services_html' ) );
    }

    // Element Mapping
    public function vc_our_services() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Our Services', 'jwa_abacus'),
                'base' => 'jwa_our_services',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'heading' => __( 'Title', 'jwa_abacus' ),
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                      'heading' => 'Services items',
                      'value' => '',
                      'type'  => 'param_group',
                      'param_name' => 'items',
                      'params' => array(
                        array(
                            'type' => 'textarea',
                            'heading' => __( 'SVG code', 'jwa_abacus' ),
                            'param_name' => 'svg_code',
                            'value' => '',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Title', 'jwa_abacus' ),
                            'param_name' => 'title',
                        ),
                        array(
                            'type' => 'textarea',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Description', 'jwa_abacus' ),
                            'param_name' => 'description',
                        ),
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Button link', 'jwa_abacus' ),
                            'param_name' => 'button_link',
                        ),

                      )
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_our_services_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'items'   => '',
                ),
                $atts
            )
        );
        //$team = vc_param_group_parse_atts($team);
				ob_start();
        $items = vc_param_group_parse_atts($items);
        ?>
        <div class="services" style="background-color: #F9F9F9;">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $title ?></h2>
                <div class="items dfr">
                  <?php foreach ($items as $item): ?>
                    <div class="item-service dfr">
                      <?php echo $item['svg_code']; ?>
                      <h3><?php echo $item['title']; ?></h3>
                      <p><?php echo $item['description']; ?></p>
                      <a href="<?php echo $item['button_link']; ?>"><?php _e('Read More', 'jwa_abacus'); ?></a>
                      <a class="link" href="<?php echo $item['button_link']; ?>"></a>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcOurServices();
