<?php
/**
 * Created 01.06.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 */


/**
 * Class IWPTeamNew
 */
class IWPTeamNew extends WPBakeryShortCode {
	
	/**
	 * IWPTeamNew constructor.
	 */
	function __construct() {
		add_action( 'init', [ $this, 'map' ], 12 );
		add_shortcode( 'iwp_team_new', [ $this, 'outputHtml' ] );
	}
	
	/**
	 * Map elements
	 *
	 * @throws Exception
	 */
	public function map() {
		
		// Stop all if VC is not enabled
		if( ! defined( 'WPB_VC_VERSION' ) ) {
			return;
		}
		
		// Map the block with vc_map()
		vc_map(
			[
				'name'     => __( 'Team', 'jwa_abacus' ),
				'base'     => 'iwp_team_new',
				'category' => __( 'JWA', 'jwa_abacus' ),
				'params'   => [
					[
						'type'        => 'dropdown',
						'heading'     => __( 'Style', 'jwa_abacus' ),
						'param_name'  => 'style',
						'value'       => [
							'Select Style' => 'one',
							'Style One'    => 'one_style',
							'Style Two'    => 'two',
						],
						'admin_label' => false,
						'save_always' => false,
						'group'       => 'General',
					],
					[
						'type'        => 'textfield',
						'heading'     => __( 'Title', 'jwa_abacus' ),
						'param_name'  => 'title',
						'value'       => '',
						'admin_label' => false,
						'save_always' => false,
						'group'       => 'General',
					],
					[
						'type'        => 'textfield',
						'heading'     => __( 'Count Output Number', 'jwa_abacus' ),
						'param_name'  => 'count',
						'value'       => '',
						'admin_label' => false,
						'save_always' => false,
						'group'       => 'General',
					],
					[
						'type'        => 'dropdown',
						'heading'     => __( 'Category', 'jwa_abacus' ),
						'param_name'  => 'category',
						'value'       => $this->getTermTeam(),
						'admin_label' => false,
						'save_always' => false,
						'group'       => 'General',
					],
				],
			]
		);
	}
	
	/**
	 * Get list Array to select element
	 *
	 * @return array
	 */
	public function getTermTeam(): array {
		$terms = get_terms( [ 'taxonomy' => 'position', 'hide_empty' => false, ] );
		if( empty( $terms ) ) {
			return [];
		}
		$termsArray                    = [];
		$termsArray['Select Category'] = 0;
		
		foreach ( $terms as $term ) {
			$termsArray[ $term->name ] = $term->term_id;
		}
		
		return $termsArray;
	}
	
	
	/**
	 * Output HTML
	 *
	 * @param      $atts
	 * @param null $content
	 *
	 * @return false|string
	 */
	public function outputHtml( $atts, $content ) {
		
		ob_start();
		
		switch ( $atts['style'] ) {
			case 'one':
				include get_template_directory() . '/include/jwa_components/template/IWPTeamNew/template_one.php';
				break;
			case 'two':
				include get_template_directory() . '/include/jwa_components/template/IWPTeamNew/template_two.php';
				break;
			default:
				include get_template_directory() . '/include/jwa_components/template/IWPTeamNew/template_one.php';
		}
		
		return ob_get_clean();
		
	}
	
}

new IWPTeamNew();