<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Content And Form
class vcContactAndForm extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_contact_and_form' ), 12);
        add_shortcode( 'jwa_contact_and_form', array( $this, 'vc_contact_and_form_html' ) );
    }

    // Element Mapping
    public function vc_contact_and_form() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        $forms = get_posts( array(
        	'numberposts' => -1,
        	'category'    => 0,
        	'orderby'     => 'date',
        	'order'       => 'DESC',
        	'include'     => array(),
        	'exclude'     => array(),
        	'meta_key'    => '',
        	'meta_value'  =>'',
        	'post_type'   => 'wpcf7_contact_form',
        	'suppress_filters' => true,
        ) );
        $select_items = array();
        foreach ($forms as $form) {
          $select_items[$form->post_title] = $form->ID;
        }
        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Content And Form', 'jwa_abacus'),
                'base' => 'jwa_contact_and_form',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textarea_html',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Content', 'jwa_abacus' ),
                        'param_name' => 'content',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Select form',  'gsklaw' ),
                        'param_name' => 'form',
                        'value' => $select_items,
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_contact_and_form_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'form'         => '',
                ),
                $atts
            )
        );

				ob_start();
        ?>
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
              <div class="content">
                <?php
                $html = $content;
                $pattern = "/<p[^>]*><\\/p[^>]*>/";
                //$pattern = "/<[^\/>]*>([\s]?)*<\/[^>]*>/";  use this pattern to remove any empty tag

                echo preg_replace($pattern, '', $html);
                //echo $content; ?>
              </div>
              <div class="sidebar">
                <?php echo do_shortcode( '[contact-form-7 id="' . $form . '"]' ) ?>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcContactAndForm();
