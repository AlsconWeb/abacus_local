<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Podcast
class vcPodcast extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_podcast' ), 12);
        add_shortcode( 'jwa_podcast', array( $this, 'vc_podcast_html' ) );
    }

    // Element Mapping
    public function vc_podcast() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Podcast', 'jwa_abacus'),
                'base' => 'jwa_podcast',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    // array(
                    //     'type' => 'textarea',
                    //     'holder' => 'div',
                    //     'heading' => __( 'Title', 'jwa_abacus' ),
                    //     'param_name' => 'title',
                    //     'admin_label' => true,
                    // ),
                    // array(
                    //   'heading' => 'Services items',
                    //   'value' => '',
                    //   'type'  => 'param_group',
                    //   'param_name' => 'items',
                    //   'params' => array(
                    //     array(
                    //         'type' => 'textarea',
                    //         'heading' => __( 'SVG code', 'jwa_abacus' ),
                    //         'param_name' => 'svg_code',
                    //         'value' => '',
                    //         'admin_label' => true,
                    //     ),
                    //     array(
                    //         'type' => 'textfield',
                    //         'holder' => 'div',
                    //         'class' => 'field-class',
                    //         'heading' => __( 'Title', 'jwa_abacus' ),
                    //         'param_name' => 'title',
                    //     ),
                    //     array(
                    //         'type' => 'textarea',
                    //         'holder' => 'div',
                    //         'class' => 'field-class',
                    //         'heading' => __( 'Description', 'jwa_abacus' ),
                    //         'param_name' => 'description',
                    //     ),
                    //     array(
                    //         'type' => 'textfield',
                    //         'holder' => 'div',
                    //         'class' => 'field-class',
                    //         'heading' => __( 'Button link', 'jwa_abacus' ),
                    //         'param_name' => 'button_link',
                    //     ),
                    //
                    //   )
                    // ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_podcast_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'items'   => '',
                ),
                $atts
            )
        );
        //$team = vc_param_group_parse_atts($team);
				ob_start();
        $items = vc_param_group_parse_atts($items);
        $sounds = get_field('sounds', 'option');
        $i = 1;
        $sounds_arr = array();
        foreach ($sounds as $sound) {
          $sounds_arr[$i-1]['track'] = $i;
          $sounds_arr[$i-1]['name'] = $sound['name'];
          $sounds_arr[$i-1]['duration'] = $sound['duration'];
          $sounds_arr[$i-1]['file'] = $sound['file'];
          $sounds_arr[$i-1]['file'] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $sound['file']);
          $sounds_arr[$i-1]['img'] = $sound['img'];
          $i++;
        }
        $js_array = json_encode($sounds_arr);

        ?>
        <script>
        <?php echo "var sound_tracks = ". $js_array . ";\n"; ?>
        </script>
        <div class="podcast">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfc">
                <h2><?php _e('Podcast' , 'jwa_abacus'); ?></h2>
                <div id="nowPlay">
                  <div id="npTitle">
                    NOW PLAY
                  </div>
                  <p id="npAction"><?php _e('Paused...' , 'jwa_abacus'); ?></p>
                  <div id="tracks"><a class="icon-prev" id="btnPrev"></a><a class="icon-next" id="btnNext"></a></div>
                </div>
                <div id="audiowrap">
                  <div id="audio0">
                    <audio id="audio1" preload="" controls="">Your browser does not support HTML5 Audio! 😢</audio>
                  </div>
                </div>
                <p><?php _e('Playlist' , 'jwa_abacus'); ?></p>
                <div id="plwrap">
                  <ul class="podcast-list"></ul>
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcPodcast();
