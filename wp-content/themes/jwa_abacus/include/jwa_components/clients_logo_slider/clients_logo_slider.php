<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Clients Logo Slider
class vcClientsLogoSlider extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_clients_logo_slider' ), 12);
        add_shortcode( 'jwa_clients_logo_slider', array( $this, 'vc_clients_logo_slider_html' ) );
    }

    // Element Mapping
    public function vc_clients_logo_slider() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Clients Logo Slider', 'jwa_abacus'),
                'base' => 'jwa_clients_logo_slider',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textfield',
                        'holder' => 'div',
                        'class' => 'field-class',
                        'heading' => __( 'Title', 'jwa_abacus' ),
                        'param_name' => 'title',
                        'admin_label' => true,
                        'weight' => 0,
                    ),
                    array(
                      'heading' => 'Icons',
                      'value' => '',
                      'type'  => 'param_group',
                      'param_name' => 'icons',
                      'params' => array(
                        array(
                            'type' => 'attach_image',
                            'heading' => __( 'Image', 'jwa_abacus' ),
                            'param_name' => 'image',
                            'value' => '',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Link', 'jwa_abacus' ),
                            'param_name' => 'icon_link',
                        ),
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Alt for image', 'jwa_abacus' ),
                            'param_name' => 'alt',
                        ),
                        array(
                            'type' => 'textarea',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Review text', 'jwa_abacus' ),
                            'param_name' => 'review_text',
                        ),
                        array(
                            'type' => 'textarea',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Author', 'jwa_abacus' ),
                            'param_name' => 'author',
                        ),
                      )
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_clients_logo_slider_html( $atts ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'icons'   => '',
                ),
                $atts
            )
        );
        $icons = vc_param_group_parse_atts($icons);

				ob_start();
        ?>
        <div class="clients">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $title ?></h2>
                <div class="clients-slider-updated">
                  <?php foreach ($icons as $icon): ?>
                    <div class="clients_slider_item">
                      <div class="review_wrapper_first">
                        <a rel=”nofollow” target="_blank" href="<?php echo $icon['icon_link'] ?>">
                          <img src="<?php echo wp_get_attachment_image_url( $icon['image'], 'full' ); ?>" alt="<?php echo $icon['alt'] ?>">
                        </a>
                        <div class="review_text">
                          "<?php echo $icon['review_text'] ?>"
                        </div>
                      </div>
                      <div class="review_wrapper_second">
                        – <?php echo $icon['author'] ?>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcClientsLogoSlider();
