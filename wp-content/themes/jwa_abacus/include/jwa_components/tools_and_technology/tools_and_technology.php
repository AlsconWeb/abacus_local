<?php

/*
* Created 24.02.20
* Version 1.0.0
* Last update 24.02.20
* Author: Maxym Y
*/

// Tools And Technology
class vcToolsAndTechnology extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_tools_and_technology' ), 12);
        add_shortcode( 'jwa_tools_and_technology', array( $this, 'vc_tools_and_technology_html' ) );
    }

    // Element Mapping
    public function vc_tools_and_technology() {

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Tools And Technology', 'jwa_abacus'),
                'base' => 'jwa_tools_and_technology',
                //'description' => __('Custom element item', 'jwa_abacus'),
                'category' => __('JWA', 'jwa_abacus'),
                'params' => array(

                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'heading' => __( 'Title', 'jwa_abacus' ),
                        'param_name' => 'title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'div',
                        'heading' => __( 'Subtitle', 'jwa_abacus' ),
                        'param_name' => 'subtitle',
                        'admin_label' => true,
                    ),
                    array(
                      'heading' => 'Items',
                      'value' => '',
                      'type'  => 'param_group',
                      'param_name' => 'items',
                      'params' => array(
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Title', 'jwa_abacus' ),
                            'param_name' => 'title',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textarea',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Description', 'jwa_abacus' ),
                            'param_name' => 'description',
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __( 'Image', 'jwa_abacus' ),
                            'param_name' => 'image',
                            'value' => '',
                        ),
                        array(
                            'type' => 'colorpicker',
                            'heading' => __( 'Color', 'text-domain' ),
                            'param_name' => 'color',
                            'value' => '',
                        ),
                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'class' => 'field-class',
                            'heading' => __( 'Link', 'jwa_abacus' ),
                            'param_name' => 'link',
                        ),
                      )
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vc_tools_and_technology_html( $atts, $content ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'title'   => '',
                    'subtitle' => '',
                    'items'   => '',
                ),
                $atts
            )
        );
        //$team = vc_param_group_parse_atts($team);
				ob_start();
        $items = vc_param_group_parse_atts($items);
        ?>
        <div class="technology" style="background-color:#F9F9F9;">
  				<div class="container">
  					<div class="row">
  						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  							<h2><?php echo $title ?></h2>
  							<p><?php echo $subtitle ?></p>
  							<div class="technology-slider">
                  <?php foreach ($items as $item): ?>
                    <a rel=”nofollow” target="_blank" class="item" href="<?php echo $item['link']; ?>">
    									<div class="img" style="color:<?php echo $item['color']; ?>">
                        <img src="<?php echo wp_get_attachment_image_url( $item['image'], 'full' ); ?>" alt="<?php echo $item['title'] ?>">
                      </div>
    									<div class="description">
    										<h3><?php echo $item['title']; ?></h3>
    										<p><?php echo $item['description']; ?></p>
    									</div>
    								</a>
                  <?php endforeach; ?>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>

				<?php
        return ob_get_clean();

    }

} // End Element Class


// Element Class Init
new vcToolsAndTechnology();
