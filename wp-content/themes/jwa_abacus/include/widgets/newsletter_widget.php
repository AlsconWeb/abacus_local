<?php

/**
 * Created 8.01.20
 * Version 1.0
 * Last update 8.01.20
 * Author: Maxym Y
 */

?>
<?php

class newsletter_widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        parent::__construct(
            'newsletter_widget', // Base ID
            'Newsletter Widget', // Name
            array( 'description' => __( 'Add a subscribe newsletter widget', 'jwa_abacus' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        extract( $args );
        $widget_id = $args['widget_id'];
        $form_id = get_field('form', 'widget_' . $widget_id);
        echo do_shortcode( '[contact-form-7 id="' . $form_id[0] . '"]' );
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {

    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

} // class Foo_Widget
?>
