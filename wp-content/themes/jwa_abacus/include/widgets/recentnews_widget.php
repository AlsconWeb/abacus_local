<?php

/**
 * Created 8.01.20
 * Version 1.0
 * Last update 8.01.20
 * Author: Maxym Y
 */

?>
<?php

class recentnews_widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    public function __construct() {
        parent::__construct(
            'recentnews_widget', // Base ID
            'Recent News Widget', // Name
            array( 'description' => __( 'Add a recent news widget', 'bmb' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        extract( $args );
        $widget_id = $args['widget_id'];
        $posts = get_field('posts', 'widget_' . $widget_id);
        ?>
        <div class="news">
          <h3><?php echo get_field('title', 'widget_' . $widget_id); ?></h3>
          <?php foreach ($posts as $id): ?>
            <?php $post = get_post($id); ?>
            <a class="post dfr" href="<?php echo get_permalink($id); ?>">
              <img src="<?php echo get_the_post_thumbnail_url( $post, 'blog_thumbnail_small_widget' ); ?>" alt="">
              <div class="description">
                <?php
                $title = substr(get_the_title($post), 0, 56);

                if (strlen($title) >= 56) {
                    //array_pop($excerpt);
                    $title = $title . '...';
                }

                $title = preg_replace('`\[[^\]]*\]`', '', $title);
                ?>
                <h3><?php echo $title; ?></h3>
              </div>
            </a>
          <?php endforeach; ?>
        </div>
        <?php
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {

    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

} // class Foo_Widget

?>
