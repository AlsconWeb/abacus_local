<?php
/**
 * Created 07.05.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$teams     = vc_param_group_parse_atts( $atts['team_elem'] );
$css_class = '';
if( isset( $atts['css'] ) ) {
	$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ) );
}
?>
<div class="team-slider <?php echo $css_class; ?>">
	<?php foreach ( $teams as $team ): ?>
		<div class="item">
			<div class="img">
				<img src="<?php echo wp_get_attachment_image_url( $team['image'], 'full' ) ?>"
				     alt="<?php echo get_the_title( $team['image'] ) ?>">
			</div>
			<div class="desc">
				<h5><?php echo $team['name'] ?><span><?php echo $team['position'] ?></span></h5>
				<ul class="dfr">
					<?php
					$certificates = vc_param_group_parse_atts( $team['certificate'] );
					foreach ( $certificates as $certificate ):
						?>
						<li><?php echo $certificate['name'] ?></li>
					<?php endforeach; ?>
				</ul>
				<p><?php echo $team['description'] ?></p>
			</div>
		</div>
	<?php endforeach; ?>
</div>