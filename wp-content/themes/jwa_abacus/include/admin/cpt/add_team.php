<?php
/**
 * Created 25.02.20
 * Version 1.0.1
 * Last update 01.06.21
 * Author: Alex L
 */


add_action( 'init', 'create_taxonomy' );
function create_taxonomy() {
	
	register_taxonomy( 'position', [ 'teams' ], [
		'label'             => '',
		'labels'            => [
			'name'              => __( 'Position', 'jwa_abacus' ),
			'singular_name'     => __( 'Position', 'jwa_abacus' ),
			'search_items'      => __( 'Search Position', 'jwa_abacus' ),
			'all_items'         => __( 'All Positions', 'jwa_abacus' ),
			'view_item '        => __( 'View Position', 'jwa_abacus' ),
			'parent_item'       => __( 'Parent Position', 'jwa_abacus' ),
			'parent_item_colon' => __( 'Parent Position:', 'jwa_abacus' ),
			'edit_item'         => __( 'Edit Position', 'jwa_abacus' ),
			'update_item'       => __( 'Update Position', 'jwa_abacus' ),
			'add_new_item'      => __( 'Add New Position', 'jwa_abacus' ),
			'new_item_name'     => __( 'New Position Name', 'jwa_abacus' ),
			'menu_name'         => __( 'Position', 'jwa_abacus' ),
		],
		'description'       => '',
		'public'            => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'hierarchical'      => true,
		'rewrite'           => true,
		'capabilities'      => [],
		'meta_box_cb'       => null,
		'show_admin_column' => true,
		'show_in_rest'      => null,
		'rest_base'         => null,
	] );
}


add_action( 'init', 'register_team' );

function register_team() {
	register_post_type( 'teams', [
		'label'              => __( 'All teams', 'jwa_abacus' ),
		'labels'             => [
			'name'               => __( 'teams', 'jwa_abacus' ),
			'singular_name'      => __( 'team', 'jwa_abacus' ),
			'add_new'            => __( 'Add team member', 'jwa_abacus' ),
			'add_new_item'       => __( 'Add team member', 'jwa_abacus' ),
			'edit_item'          => __( 'Edit team member', 'jwa_abacus' ),
			'new_item'           => __( 'New team member', 'jwa_abacus' ),
			'view_item'          => __( 'View team member', 'jwa_abacus' ),
			'search_items'       => __( 'Search team member', 'jwa_abacus' ),
			'not_found'          => __( 'teams not found', 'jwa_abacus' ),
			'not_found_in_trash' => __( 'teams not found', 'jwa_abacus' ),
			'parent_item_colon'  => __( 'teams', 'jwa_abacus' ),
			'menu_name'          => __( 'All teams', 'jwa_abacus' ),
		],
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		//'rewrite'            => array('slug' => 'our-people'),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => true,
		'menu_position'      => 10,
		'supports'           => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions' ],
		'menu_icon'          => 'dashicons-businessman',
	] );
}


