<?php
/**
 * Created 15.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

//use QuickBooksOnline\API\DataService\DataService;

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\QueryFilter\QueryMessage;

add_action( 'wp_ajax_get_invoice', 'get_invoice' );
add_action( 'wp_ajax_nopriv_get_invoice', 'get_invoice' );

function get_invoice () {
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'invoice' ) ) {
		wp_send_json_success( [ 'error' => 'Nonce Code no success' ] );
	}
	
	$invid = $_POST['invid'];
	
	$clientID     = get_option( 'qb_client_id', false );
	$clientSecret = get_option( 'qb_client_secret', false );
	$token        = get_option( 'jwa_qb_token', false );
	$refreshToken = get_option( 'jwa_refresh_token', false );
	$realmID      = get_option( 'jwa_realm_id', false );
	$version      = get_option( 'qb_version', false );
	
	$dataService = DataService::Configure( [
		'auth_mode'       => 'oauth2',
		'ClientID'        => $clientID,
		'ClientSecret'    => $clientSecret,
		'accessTokenKey'  => $token,
		'refreshTokenKey' => $refreshToken,
		'QBORealmID'      => $realmID,
		'baseUrl'         => $version ? $version : 'development',
	] );

//	error log
	$dataService->disableLog();
	$dataService->throwExceptionOnError( false );
	
	// Build a query
	$oneQuery         = new QueryMessage();
	$oneQuery->sql    = "SELECT";
	$oneQuery->entity = "Invoice";
	
	// Run a query
	$queryString = $oneQuery->getString();
	$queryString = $queryString . " where DocNumber = '" . $invid . "'";
	$entities    = $dataService->Query( $queryString );
	
	$error = $dataService->getLastError();
	
	$customer = $dataService->FindbyId( 'customer', $entities[0]->CustomerRef );
	
	
	if ( $error ) {
		wp_send_json_error( [
			'code'     => "The Status code is: " . $error->getHttpStatusCode(),
			'message'  => "Please verify the invoice number you are typing is the one you received from us by email. If you still cannot find the invoice, please contact us.",
			'response' => "The Response message is: " . $error->getResponseBody(),
		] );
	} else {
		if ( $entities !== null ) {
			if ( $entities[0]->Balance == '0' ) {
				wp_send_json_error( [ 'code' => '', 'message' => 'This invoice has already been paid.' ] );
			} else {
				
				$content = 'Adress: ' . $entities[0]->BillAddr->Line1 . ' ' . $entities[0]->BillAddr->City . ' ' .
				           $entities[0]->BillAddr->Country . '<br>';
				$content .= 'Customer: ' . $customer->Title . ' ' . $customer->GivenName . ' ' . $customer->FamilyName . '<br>';
				$content .= 'Customer Email :' . $customer->PrimaryEmailAddr->Address . '<br>';
				
				$data = [
					'name'          => 'Invoice #' . $entities[0]->DocNumber,
					'description'   => $content,
					'regular_price' => (float) $entities[0]->TotalAmt,
				];
				
				$request = new WP_REST_Request( 'POST' );
				$request->set_body_params( $data );
				$products_controller = new WC_REST_Products_Controller;
				$response            = $products_controller->create_item( $request );
				
				$dataCustomerArr = [
					'customer_name'    => $customer->Title . ' ' . $customer->GivenName . ' ' . $customer->FamilyName,
					'customer_email'   => $customer->PrimaryEmailAddr->Address,
					'customer_address' => [
						'line_1'     => $customer->BillAddr->Line1,
						'line_2'     => $customer->BillAddr->Line2,
						'line_3'     => $customer->BillAddr->Line3,
						'line_4'     => $customer->BillAddr->Line4,
						'city'       => $customer->BillAddr->City,
						'postalCode' => $customer->BillAddr->PostalCode,
					],
				];
				
				add_post_meta( $response->data['id'], 'jwa_invoice_customer', $dataCustomerArr, true );
				add_post_meta( $response->data['id'], 'jwa_invoice_id', $entities[0]->Id, true );
				$CustomerRef = add_post_meta( $response->data['id'], 'jwa_customer_ref', $entities[0]->CustomerRef, true );
				
				if ( is_wp_error( $response ) ) {
					wp_send_json_error( [ 'error' => 'fatalerror' ] );
				} else {
					wp_send_json_success( [ 'prodID' => $response->data['id'], '$CustomerRef' => $CustomerRef ] );
				}
			}
		} else {
			wp_send_json_error( [ 'code' => '404', 'message' => 'Not Find' ] );
		}
	}
	
}