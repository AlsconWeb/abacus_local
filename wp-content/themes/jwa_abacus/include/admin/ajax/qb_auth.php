<?php
/**
 * Created 22.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

use QuickBooksOnline\API\DataService\DataService;

add_action( 'wp_ajax_qb_auth', 'jwa_qb_auth' );
add_action( 'wp_ajax_nopriv_qb_auth', 'jwa_qb_auth' );

function jwa_qb_auth () {
	$clientID     = get_option( 'qb_client_id', false );
	$clientSecret = get_option( 'qb_client_secret', false );
	$redirectURI  = get_option( 'qb_redirect_url', false );
	$version      = get_option( 'qb_version', false );
	
	$dataService = DataService::Configure( [
		'auth_mode'    => 'oauth2',
		'ClientID'     => $clientID,
		'ClientSecret' => $clientSecret,
		'RedirectURI'  => $redirectURI,
		'scope'        => 'com.intuit.quickbooks.accounting openid profile email phone address',
		'baseUrl'      => $version ? $version : "development",
	] );
	$dataService->disableLog();
	$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
	$parseUrl          = parseAuthRedirectUrl( $_SERVER['QUERY_STRING'] );
	
	/*
	 * Update the OAuth2Token
	 */
	$accessToken = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken( $parseUrl['code'], $parseUrl['realmId'] );
	$dataService->updateOAuth2Token( $accessToken );
	
	$token         = get_option( 'jwa_qb_token', false );
	$refreshToken  = get_option( 'jwa_refresh_token', false );
	$xRefreshToken = get_option( 'jwa_x_refresh_token', false );
	$expiresIn     = get_option( 'jwa_expires_in', false );
	$realmID       = get_option( 'jwa_realm_id', false );
	
	if ( $token ) {
		$token = update_option( 'jwa_qb_token', $accessToken->getAccessToken(), 'no' );
	} else {
		$token = add_option( 'jwa_qb_token', $accessToken->getAccessToken(), '', 'no' );
	}
	
	if ( $refreshToken ) {
		$refreshToken = update_option( 'jwa_refresh_token', $accessToken->getRefreshToken(), 'no' );
	} else {
		add_option( 'jwa_refresh_token', $accessToken->getRefreshToken(), '', 'no' );
	}
	
	if ( $xRefreshToken ) {
		update_option( 'jwa_x_refresh_token', $accessToken->getRefreshTokenExpiresAt(), 'no' );
	} else {
		add_option( 'jwa_x_refresh_token', $accessToken->getRefreshTokenExpiresAt(), '', 'no' );
	}
	
	if ( $expiresIn ) {
		update_option( 'jwa_expires_in', $accessToken->getAccessTokenExpiresAt(), 'no' );
	} else {
		add_option( 'jwa_expires_in', $accessToken->getAccessTokenExpiresAt(), '', 'no' );
	}
	
	if ( $realmID ) {
		update_option( 'jwa_realm_id', $parseUrl['realmId'], 'no' );
	} else {
		add_option( 'jwa_realm_id', $parseUrl['realmId'], '', 'no' );
	}
	
	/*
	 * Setting the accessToken for session variable
	 */
	$_SESSION['sessionAccessToken1'] = $accessToken;
	
	wp_send_json_success( [ 'token' => $token, ] );
}

