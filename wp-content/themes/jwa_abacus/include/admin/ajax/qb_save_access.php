<?php
/**
 * Created 23.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

add_action( 'wp_ajax_qb_save_access', 'jwa_qb_save_access' );
add_action( 'wp_ajax_nopriv_qb_save_access', 'jwa_qb_save_access' );

function jwa_qb_save_access () {
	$clientID     = base64_decode( $_POST['clientID'] );
	$clientSecret = base64_decode( $_POST['clientSecret'] );
	$redirectURI  = $_POST['redirectURI'];
	$version      = $_POST['version'];
	
	if ( str_replace( ' ', '', $clientID ) == '' ) {
		wp_send_json_error( [ 'error' => 'Client ID empty' ] );
	}
	if ( str_replace( ' ', '', $clientSecret ) == '' ) {
		wp_send_json_error( [ 'error' => 'Client Secret empty' ] );
	}
	if ( str_replace( ' ', '', $redirectURI ) == '' ) {
		wp_send_json_error( [ 'error' => 'Redirect URI empty' ] );
	}
	
	$clientIDOption     = get_option( 'qb_client_id', false );
	$clientSecretOption = get_option( 'qb_client_secret', false );
	$redirectURIOption  = get_option( 'qb_redirect_url', false );
	$versionOption      = get_option( 'qb_version', false );
	
	if ( $clientIDOption ) {
		update_option( 'qb_client_id', $clientID, 'no' );
	} else {
		add_option( 'qb_client_id', $clientID, '', 'no' );
	}
	
	if ( $clientSecretOption ) {
		update_option( 'qb_client_secret', $clientSecret, 'no' );
	} else {
		add_option( 'qb_client_secret', $clientSecret, '', 'no' );
	}
	
	if ( $redirectURIOption ) {
		update_option( 'qb_redirect_url', $redirectURI, 'no' );
	} else {
		add_option( 'qb_redirect_url', $redirectURI, '', 'no' );
	}
	
	if ( $versionOption ) {
		$ver = update_option( 'qb_version', $version, 'no' );
	} else {
		$ver = add_option( 'qb_version', $version, '', 'no' );
	}
	
	wp_send_json_success( [
		'clientID'     => $clientIDOption,
		'clientSecret' => $clientSecretOption,
		'redirectURI'  => $redirectURIOption,
		'version'      => $ver,
	] );
}