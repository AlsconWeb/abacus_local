<?php
/**
 * Created 28.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */


use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Payment;

add_action( 'woocommerce_thankyou', 'payment_update_invoice_status' );


function payment_update_invoice_status ( $order_id ) {
	
	
	$clientID     = get_option( 'qb_client_id', false );
	$clientSecret = get_option( 'qb_client_secret', false );
	$token        = get_option( 'jwa_qb_token', false );
	$refreshToken = get_option( 'jwa_refresh_token', false );
	$realmID      = get_option( 'jwa_realm_id', false );
	$version      = get_option( 'qb_version', false );
	
	$dataService = DataService::Configure( [
		'auth_mode'       => 'oauth2',
		'ClientID'        => $clientID,
		'ClientSecret'    => $clientSecret,
		'accessTokenKey'  => $token,
		'refreshTokenKey' => $refreshToken,
		'QBORealmID'      => $realmID,
		'baseUrl'         => $version ? $version : 'development',
	] );

//	$errorLogDir = wp_get_upload_dir();
//	$dataService->setLogLocation( $errorLogDir['basedir'] );
	$dataService->disableLog();
	$dataService->throwExceptionOnError( true );
//Add a new Invoice
	$order    = wc_get_order( $order_id );
	$items    = $order->get_items();
	$totalAmt = $order->get_total();
//	var_dump( $TotalAmt );
	
	foreach ( $items as $item ) {
		$product_id  = $item->get_product_id();
		$invID       = get_post_meta( $product_id, 'jwa_invoice_id', true );
		$customerRef = get_post_meta( $product_id, 'jwa_customer_ref', true );
	}
//	var_dump( $customerRef );
	$theResourceObj = Payment::create( [
		"CustomerRef" =>
			[
				"value" => $customerRef,
			],
		"TotalAmt"    => $totalAmt,
		"Line"        => [
			[
				"Amount"    => $totalAmt,
				"LinkedTxn" => [
					[
						"TxnId"   => $invID,
						"TxnType" => "Invoice",
					],
				],
			],
		],
	] );
	$resultingObj   = $dataService->Add( $theResourceObj );
	$resultingObj   = $dataService->Update( $theResourceObj );
	$error          = $dataService->getLastError();
	
	if ( $error ) {
		echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
		echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
		echo "The Response message is: " . $error->getResponseBody() . "\n";
	} else {
		echo '<h3 class="pay_success">The payment was successfully credited.</h3>';
	}
	
}