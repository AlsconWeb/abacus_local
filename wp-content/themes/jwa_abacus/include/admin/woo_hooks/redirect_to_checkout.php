<?php
/**
 * Created 28.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );

function bbloomer_redirect_checkout_add_cart () {
	return wc_get_checkout_url();
}