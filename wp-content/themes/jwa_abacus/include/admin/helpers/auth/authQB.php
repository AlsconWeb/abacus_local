<?php
/**
 * Created 20.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

use QuickBooksOnline\API\DataService\DataService;


$dataService = DataService::Configure( [
	'auth_mode'    => 'oauth2',
	'ClientID'     => "ABhUlJ8v3Mje6ZLIAEMjGKcgX9l77QmYGX4Qv2bdSKQUZ8RW7g",
	'ClientSecret' => "SpvqB7AHOcLFxxRUhCf1soyRnX4BFPu5OUBu6Z6N",
	'RedirectURI'  => "https://developer.intuit.com/v2/OAuth2Playground/RedirectUrl",
	'scope'        => "com.intuit.quickbooks.accounting",
	'baseUrl'      => "Development",
] );
$dataService->disableLog();
$OAuth2LoginHelper    = $dataService->getOAuth2LoginHelper();
$authorizationCodeUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();
$parseUrl             = parseAuthRedirectUrl( $authorizationCodeUrl );
header( 'Location:' . $authorizationCodeUrl );
