<?php
/**
 * Created 02.06.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

add_action( 'send_emails', 'send_email', 10, 1 );
function send_email ( $order ) {
	$transaction_id = $order->get_transaction_id();
	foreach ( $order->get_items() as $item ) {
		$product_name = $item['name'];
		
		$product       = $item->get_product();
		$product_id    = $item->get_product_id();
		$price         = $product->get_price();
		$custormerData = get_post_meta( $product_id, 'jwa_invoice_customer', true );
		break;
	}
	ob_start();
	?>
	<div class="woocommerce-order thank-page">
	
	<table border="0" cellpadding="0" cellspacing="0" width="600"
	       style="background-color:#ffffff;border:1px solid #dedede;border-radius:3px">
		<tbody>
		<tr>
			<td align="center" valign="top">
				
				<table border="0" cellpadding="0" cellspacing="0" width="100%" id="m_-4116416221049578367template_header"
				       style="background-color:#282828;color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;border-radius:3px 3px 0 0">
					<tbody>
					<tr>
						<td style="padding:36px 48px;display:block">
							<h1
								style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:30px;font-weight:300;line-height:150%;margin:0;text-align:left;color:#ffffff">
								Receipt for payment <?php echo $order->get_id(); ?></h1>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="600">
					<tbody>
					<tr>
						<td valign="top" style="background-color:#ffffff">
							
							<table border="0" cellpadding="20" cellspacing="0" width="100%">
								<tbody>
								<tr>
									<td valign="top" style="padding:48px 48px 32px">
										<div
											style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">
											<p style="margin:0 0 16px">Here are the details of your order placed
												on <?php echo get_the_time( 'M j, Y', $order->get_id() ); ?>: </p>
											<h2
												style="color:#2B368D;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">
												Receipt for payment [<?php echo $order->get_id(); ?>]
												(<?php echo get_the_time( 'M j, Y', $order->get_id() );
												?>)</h2>
											<div style="margin-bottom:40px">
												<table cellspacing="0" cellpadding="6" border="1"
												       style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;width:100%;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
													<thead>
													<tr>
														<th scope="col"
														    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">
															Service
														</th>
														<th scope="col"
														    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">
															Quantity
														</th>
														<th scope="col"
														    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">
															Price
														</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td
															style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;word-wrap:break-word">
															<?php echo $product_name ?>
														</td>
														<td
															style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
															1
														</td>
														<td
															style="color:#636363;border:1px solid #e5e5e5;padding:12px;text-align:left;vertical-align:middle;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif">
															<span><span>$</span><?php echo number_format( $price ); ?></span></td>
													</tr>
													</tbody>
													<tfoot>
													<tr>
														<th scope="row" colspan="2"
														    style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">
															Total:
														</th>
														<td
															style="color:#636363;border:1px solid #e5e5e5;vertical-align:middle;padding:12px;text-align:left">
															<span><span>$</span><?php echo $order->get_total(); ?></span></td>
													</tr>
													</tfoot>
												</table>
												
												<style>
                            .customers-table {
                                width: 100%;
                                margin: 10px 0 20px 0;
                            }

                            .heding-bill {
                                margin-top: 20px;
                            }

                            .customers-table td, .customers-table th {
                                border: 1px solid #E5E5E5;
                                padding: 10px;
                            }
												</style>
												
												<p class="heding-bill">Bill info</p>
												<table class="customers-table">
													<thead>
													<tr>
														<th scope="col">Name</th>
														<th scope="col">Email</th>
														<th scope="col">Billing address</th>
													</tr>
													<tbody>
													<tr>
														<td><?php echo $custormerData['customer_name']; ?></td>
														<td><?php echo $custormerData['customer_email'] ?></td>
														<td><?php echo $custormerData['customer_address']['line_1'] ?></td>
													</tr>
													</tbody>
													</thead>
												</table>
												<?php if ( $transaction_id ): ?>
													<p>Charge ID:<?php echo $transaction_id; ?></p>
												<?php endif; ?>
											</div>
											
											<div style="display:none;font-size:0;max-height:0;line-height:0;padding:0"></div>
										
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<?php
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'From: info@abacusgroup.ca' . "\r\n";
	$headers .= 'Content-type:text/html; charset=utf-8' . "\r\n";
	$to      = $custormerData['customer_email'] . ', ' . 'accounting@abacusgroup.ca';
	mail( $to, $product_name, ob_get_clean(), $headers );
	
}