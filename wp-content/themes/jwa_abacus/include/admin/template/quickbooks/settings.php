<?php
/**
 * Created 20.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

use QuickBooksOnline\API\DataService\DataService;

$clientID     = get_option( 'qb_client_id', false );
$clientSecret = get_option( 'qb_client_secret', false );
$redirectURI  = get_option( 'qb_redirect_url', false );
$version      = get_option( 'qb_version', false );

$dataService = DataService::Configure( [
	'auth_mode'    => 'oauth2',
	'ClientID'     => $clientID,
	'ClientSecret' => $clientSecret,
	'RedirectURI'  => 'https://abacusgroup.ca/wp-admin/admin-ajax.php?action=qb_auth',
	'scope'        => 'com.intuit.quickbooks.accounting openid profile email phone address',
	'baseUrl'      => $version ? $version : 'development',
] );

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$authUrl           = $OAuth2LoginHelper->getAuthorizationCodeURL();

// Store the url in PHP Session Object;
$_SESSION['authUrl'] = $authUrl;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
	
	var url = '<?php echo $authUrl; ?>';
	
	var OAuthCode = function (url) {
		
		this.loginPopup = function (parameter) {
			this.loginPopupUri(parameter);
		}
		
		this.loginPopupUri = function (parameter) {
			
			// Launch Popup
			var parameters = "location=1,width=800,height=650";
			parameters += ",left=" + (screen.width - 800) / 2 + ",top=" + (screen.height - 650) / 2;
			
			var win = window.open(url, 'connectPopup', parameters);
			var pollOAuth = window.setInterval(function () {
				try {
					if (win.document.URL.indexOf("code") != -1) {
						window.clearInterval(pollOAuth);
						win.close();
						location.reload();
					}
				} catch (e) {
					console.log(e)
				}
			}, 100);
		}
	}
	var oauth = new OAuthCode(url);
</script>
<style>
	.form-check {
		display: -webkit-flex;
		display: -moz-flex;
		display: -ms-flex;
		display: -o-flex;
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		align-items: center;
		padding-left: 0;
	}
	
	.form-check .form-check-input {
		position: static;
		margin: 0 5px 0px 0;
	}
</style>
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md">
				<h1>Api Settings</h1>
				<form class="bq_setting">
					<div class="form-group">
						<label for="clientID">Client ID</label>
						<input type="<?php echo $clientID ? "password" : 'text' ?>" class="form-control" id="clientID"
						       name="clientID"
						       value="<?php
						       echo
						       $clientID ? $clientID : '' ?>"
						       aria-describedby="clientIDHelp"
						       required>
						<small id="clientIDHelp" class="form-text text-muted">Enter the Client ID of your App</small>
					
					</div>
					<div class="form-group">
						<label for="clientSecret">Client Secret</label>
						<input type="<?php echo $clientSecret ? 'password' : 'text' ?>" class="form-control" id="clientSecret"
						       name="clientSecret" value="<?php echo $clientSecret ? $clientSecret : ''; ?>"
						       required>
					</div>
					<div class="form-group">
						<label for="RedirectURI">RedirectURI</label>
						<input type="text" class="form-control" id="RedirectURI" name="redirectURI" value="<?php echo
						$redirectURI ? $redirectURI : '' ?>" required>
					</div>
					
					<div class="form-check">
						<input class="form-check-input" type="radio" name="version_api" id="version_api_dev" value="development"
							<?php echo $version == 'development' ? 'checked' : '' ?>>
						<label class="form-check-label" for="version_api_dev">
							Development
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="version_api" id="version_api_prod"
						       value="production" <?php echo $version == 'production' ? 'checked' : '' ?>>
						<label class="form-check-label" for="version_api_prod">
							Product Live
						</label>
					</div>
					
					<button type="submit" class="btn btn-primary">Submit</button>
					<?php if ( get_option( 'jwa_qb_token', false ) ): ?>
						<a class="btn btn-success get_token" role="button" onclick="oauth.loginPopup()"
						   style="color: #fff; margin-left: 10px;">
							Get Token
						</a>
					
					<?php else: ?>
						<a class="btn btn-success get_token disabled" role="button" aria-disabled="true"
						   style="color: #fff; margin-left: 10px;"
						   onclick="oauth.loginPopup()">Get
							Token
						</a>
					<?php endif; ?>
				</form>
			</div>
		</div>
	</div>
</div>