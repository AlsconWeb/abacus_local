<?php
/**
 * Created 23.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

use QuickBooksOnline\API\DataService\DataService;

add_action( 'wp_head', 'addCroneQB' );
add_action( 'jwa_token_update', 'updateData' );

function addCroneQB () {
	if ( ! wp_next_scheduled( 'jwa_token_update' ) ) {
		wp_schedule_single_event( time() + 900, 'jwa_token_update' );
	}
}


function updateData () {
	$clientID     = get_option( 'qb_client_id', false );
	$clientSecret = get_option( 'qb_client_secret', false );
	$redirectURI  = get_option( 'qb_redirect_url', false );
	$realmID      = get_option( 'jwa_realm_id', false );
	$version      = get_option( 'qb_version', false );
	
	if ( $clientID && $clientSecret && $redirectURI && $realmID ) {
		$accessToken = get_option( 'jwa_refresh_token', false );
		$dataService = DataService::Configure( [
			'auth_mode'       => 'oauth2',
			'ClientID'        => $clientID,
			'ClientSecret'    => $clientSecret,
			'RedirectURI'     => $redirectURI,
			'baseUrl'         => $version ? $version : 'development',
			'refreshTokenKey' => $accessToken,
			'QBORealmID'      => $realmID,
		] );
		$dataService->disableLog();
		/*
     * Update the OAuth2Token of the dataService object
     */
		$OAuth2LoginHelper       = $dataService->getOAuth2LoginHelper();
		$refreshedAccessTokenObj = $OAuth2LoginHelper->refreshToken();
		$dataService->updateOAuth2Token( $refreshedAccessTokenObj );
		
		update_option( 'jwa_qb_token', $refreshedAccessTokenObj->getAccessToken(), 'no' );
		update_option( 'jwa_refresh_token', $refreshedAccessTokenObj->getRefreshToken(), 'no' );
		update_option( 'jwa_x_refresh_token', $refreshedAccessTokenObj->getRefreshTokenExpiresAt(), 'no' );
		update_option( 'jwa_expires_in', $refreshedAccessTokenObj->getAccessTokenExpiresAt(), 'no' );
		
	} else {
		wp_send_json_error( [ 'message' => 'Check token receipt' ] );
	}
}