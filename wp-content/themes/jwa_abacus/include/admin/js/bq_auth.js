jQuery(document).ready(($) => {
	$(".bq_setting").submit(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'qb_save_access',
			clientID: window.btoa($(this).find('input[name=clientID]').val()),
			redirectURI: $(this).find('input[name=redirectURI]').val(),
			clientSecret: window.btoa($(this).find('input[name=clientSecret]').val()),
			version: $(this).find('input[name=version_api]:checked').val()
		}
		console.log(data);
		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			success: function (res) {
				console.log(res);
				if (res.success) {
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'Accesses saved successfully',
						showConfirmButton: false,
						timer: 1500
					})
					$('.get_token').attr('aria-disabled', 'false').removeClass('disabled');
				}
				// window.location.replace(res.data.urlRedirect);
			}
		});
	});
	
});