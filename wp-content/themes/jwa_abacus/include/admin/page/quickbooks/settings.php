<?php
/**
 * Created 20.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

add_action('admin_menu', 'my_plugin_menu');
function my_plugin_menu() {
	add_menu_page('QuickBooks Settings', 'QuickBooks', 'manage_options', 'quickbooks', 'quickbooks_settings', 'dashicons-money', '5');
}

function quickbooks_settings(){
	require_once get_template_directory() . '/include/admin/template/quickbooks/settings.php';
}