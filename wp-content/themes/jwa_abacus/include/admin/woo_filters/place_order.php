<?php
/**
 * Created 28.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

add_filter( 'woocommerce_order_button_text', 'jwa_text_button', 50, 1 );

function jwa_text_button ( $text ) {
	return __( 'Proceed with payment', 'woocommerce' );
}