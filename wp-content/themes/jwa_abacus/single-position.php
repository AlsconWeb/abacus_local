<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="blogs position">
	

<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dfr">
		<div class="content">
			<?php the_content(); ?>
		</div>
		<div class="sidebar">
			<?php
				  if ( function_exists('dynamic_sidebar') )
					dynamic_sidebar('news-sidebar');
			 ?>
		</div>
	</div>
</div>

</div>
<?php endwhile; ?>
<?php get_footer(); ?>